(function($){
    $( function() {
      $( ".webz-qa-accordion" ).accordion({
        heightStyle: "content" ,
        collapsible: true,
        active: false,
      });
    } );
      //accordion

  $('.toggle').click(function(e) {
    e.preventDefault();
    $('.webz-qa-accordion .toggle').removeClass('active');
  var $this = $(this);

  
  if ($this.next().hasClass('show')) {
      $this.next().removeClass('show');
      $this.next().slideUp(350);
    
  } else {
      $this.parent().parent().find('li .inner').removeClass('show');
      $this.parent().parent().find('li .inner').slideUp(350);
      $this.next().toggleClass('show');
      $this.next().slideToggle(350);
      $this.addClass('active');
  }
});
})(jQuery);