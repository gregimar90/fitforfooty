<?php

$webz_last_map_id = NULL;

vc_map(
    array(
        'name' => 'Google Map',
        'base' => 'webz_google_map',
        'as_parent' => array('only' => 'webz_google_map_item'),
        'description' => __('Content slider', 'webz-vc-google-map'), 
        'category' => __('WEBZ'),   
        'is_container' => false,
        'content_element' => true,
        'icon' => plugin_dir_url( __FILE__ ) . 'assets/img/icon.png',
        'params' => array(               
            array(
                'type' => 'textfield',
                'heading' => __('Google Maps API key', 'webz-vc-google-map'), 
                'param_name' => 'api_key',
                'description' => __('Google Maps API key')
            ),
            array(
                'type' => 'textfield',
                'heading' => __('Map height', 'webz-vc-google-map'), 
                'param_name' => 'height',
                'std' => '400px',
                'description' => __('Map height')
            ),
            array(
                'type' => 'textfield',
                'heading' => __('Map width', 'webz-vc-google-map'), 
                'param_name' => 'width',
                'std' => '100%',
                'description' => __('Map width')
            ), 
            array(
                'type' => 'textfield',
                'heading' => __('Default marker image size (px)', 'webz-vc-google-map'), 
                'param_name' => 'marker_size',
                'std' => '50',
                'description' => __('Default marker image size (px)')
            ), 
            array(
                'type' => 'textfield',
                'heading' => __('Custom class', 'webz-vc-google-map'),
                'param_name' => 'custom_class',
                'value' => '',
                'description' => __('Map custom class')
            ),
            
            array(
                'type' => 'textarea_raw_html',
                'heading' => __('Custom styles', 'webz-vc-google-map'),
                'param_name' => 'custom_styles',
                'value' => '',
                'description' => __('Map custom styles in JSON'),
                'group' => __( 'Design options', 'webz-vc-google-map'),
            ),
            
            array(
                'type' => 'css_editor',
                'heading' => __( 'Css'),
                'param_name' => 'css',
                'group' => __( 'Design options', 'webz-vc-google-map'),
            )           
        ),
        'js_view' => 'VcColumnView'
    )
);   

vc_map( array(
    'name' => __( 'Marker', 'webz-vc-google-map' ),
    'base' => 'webz_google_map_item',
    'category' => __( 'WEBZ' ), 
    'icon' => plugin_dir_url( __FILE__ ) . 'assets/img/icon2.png',
    'as_child' => array('only' => 'webz_google_map'),
    'params' => array(
        array(
            'type' => 'textfield',
            'heading' => __('Latitude', 'webz-vc-google-map'),
            'param_name' => 'lat',
            'value' => '',
            'description' => __('Latitude'),
        ),
        array(
            'type' => 'textfield',
            'heading' => __('Longitude', 'webz-vc-google-map'),
            'param_name' => 'lng',
            'value' => '',
            'description' => __('Longitude')
        ),
        array(
            'type' => 'textarea_html',
            'heading' => __('Marker description', 'webz-vc-google-map'),
            'param_name' => 'content',
            "value" => __( "<p>I am test text block. Click edit button to change this text.</p>", "my-text-domain" ),
            'description' => __('Marker description'),
            "holder" => "div",
        ),
        array(
            'type' => 'attach_image',
            'heading' => __('Marker Image', 'webz-vc-gallery-slider'), 
            'param_name' => 'image',
            'description' => __('Marker Image (empty = default image)'),
            'value' => '',
            'admin_label' => true
        ),  
        array(
            'type' => 'textfield',
            'heading' => __('Size (px)', 'webz-vc-google-map'),
            'param_name' => 'size',
            'description' => __('Marker size in px'),
            'dependency' => array(
                'element' => 'image',
                'not_empty' => true
            ),
        ),
        array(
            'type' => 'checkbox',
            'heading' => __( 'Rounded marker image'),
            'param_name' => 'rounded',
            'value' => array(' ' => true),
            'description' => __( 'Rounded marker image', 'webz-vc-gallery-slider'),
            'std' => false,
            'dependency' => array(
                'element' => 'image',
                'not_empty' => true
            ),
        ),
    )
) );

function webz_google_map_output( $atts, $content )
{   
    global $webz_last_map_id;
    
    extract(
        shortcode_atts(
            array(
                'width' => '',
                'height' => '',
                'marker_size' => '',
                'api_key' => '',
                'custom_class' => '',
                'custom_styles' => '',
                'css' => ''
            ), 
            $atts
        )
    );
    
    webz_enqueue_bootstrap();
    
    wp_enqueue_script( 'webz-google-map-js' , plugin_dir_url( __FILE__ ).'assets/js/script.js' );    
    wp_enqueue_script( 'google-maps' , 'https://maps.googleapis.com/maps/api/js?key=' . $api_key . '&callback=webz_google_maps_init', array( 'webz-google-map-js' ) );
   
    $js = '';
    

    $js .= '<script>if( typeof( webz_google_maps ) === "undefined" ){var webz_google_maps=[];var webz_google_maps_markers=[];}</script>' . PHP_EOL;

    
    $map_id = webz_generate_unique_id( 'google-map' );
    
    $webz_last_map_id = $map_id;
    $width = $width ? $width : '100%';
    $height = $height ? $height : '400px';
    $marker_size = $marker_size ? $marker_size : '50';
    $custom_styles = $custom_styles ? urldecode( base64_decode( $custom_styles ) ) : "''";
    $custom_class = $custom_class ? $custom_class : null;
    
    $js .= <<<JS
    <script>            
        webz_google_maps.push({
            id: '$map_id',
            width: '$width',
            height: '$height',
            marker_size: '$marker_size',
            custom_styles: $custom_styles            
        });
    </script>
JS;
    
    $css = "<style>#$map_id{width: $width;height: $height;}</style>";
            
    $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ) );
    
    if( $custom_class ){
        $css_class .= ' ' . $custom_class;
    }
    
    $output = $js . $css . '<div id="' . $map_id . '" class="' . $css_class . '"></div>' . do_shortcode( $content );
    
    return $output;    
} 

function webz_google_map_item_output( $atts, $content )
{   
    global $webz_last_map_id;
    
    extract(
        shortcode_atts(
            array(
                'lat' => '',
                'lng' => '',
                'size' => '',
                'rounded' => '',
                'image' => ''
            ), 
            $atts
        )
    );
    
    $image = $image ? '"' . webz_image_url( $image, 'full' ) . '"' : 'null';
    $size = $size ? '"' . $size . '"' : 'null';
    $rounded = $rounded ? true : false;
    
    $_content = str_replace( PHP_EOL, '', $content );

    $css = $js = "";
    
    if( $rounded ){
        $css .= "<style>#$webz_last_map_id img[src=$image]{border-radius:50%;}</style>";
    }
    
    $js .= <<<JS
    <script>            
        if(typeof(webz_google_maps_markers['$webz_last_map_id']) === 'undefined'){
            webz_google_maps_markers['$webz_last_map_id'] = [];
        }

        webz_google_maps_markers['$webz_last_map_id'].push({
            lat: '$lat',
            lng: '$lng',
            size: $size,
            content: '$_content',
            image: $image
        });
    </script>
JS;
    
    return $css . $js;    
} 

add_shortcode( 'webz_google_map', 'webz_google_map_output');
add_shortcode( 'webz_google_map_item', 'webz_google_map_item_output');

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_Webz_Google_Map extends WPBakeryShortCodesContainer{}
}