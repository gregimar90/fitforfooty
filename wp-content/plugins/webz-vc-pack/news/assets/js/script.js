(function($) {
    
    var container = $(document).find('.webz-news');
    var news_container = container.find( '.news-container' );
    var loader = news_container.find('.webz-news-loader');
    
    $(document).on('click', '.filter-news', function(e) {      
        
        var _this = $(this);
        
        e.preventDefault();
        
        loader = news_container.find('.webz-news-loader');
        
        loader.show();
        $(document).find('.webz-news-pagination').remove();
        $(document).find('.webz-news-no-news').remove();
        
        var cat_url = $(this).attr('href');
        
        news_container.find('.webz-news-item').hide();
        
        $.get( cat_url, { ajax: true }, function( html ) {
            news_container.empty().html( html );           
            
            if(_this.hasClass('filter-category')) {
                clearActiveCategories( container );
                _this.addClass('active');
                news_container.find('.webz-news-item').fadeIn('slow');
            }
        } )
        .always(function() {
            //loader.hide();
        });
        
    });    
    
    function clearActiveCategories( container ) {
        container.find('.filter-category').removeClass('active');
    }
    
})(jQuery);