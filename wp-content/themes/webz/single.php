<?php
get_header();

$views = webz_custom_field_value( 'views' , $post -> ID );

if( !$views ) {
    $views = 0;
}

$new_views = $views + 1;

update_post_meta( $post -> ID, 'views', $new_views );

$vc = $post && preg_match( '/vc_row/', $post -> post_content ) ? true : false;

?>

<?php 

if( $vc ) :
    echo apply_filters('the_content', $post->post_content);
else:
    ?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-12"><?php echo apply_filters( 'the_content', $post -> post_content )?></div>
        </div>            
    </div>
</section>
    <?php
endif;


get_footer();
