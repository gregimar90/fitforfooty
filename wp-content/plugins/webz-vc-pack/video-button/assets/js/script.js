(function($){

$.each(webz_video_buttons, function(i, button){    

    var modal_html = '<div class="modal fade webz-video-button-modal" id="'+button.modal_id+'" tabindex="-1" role="dialog" aria-labelledby="'+button.modal_id+'" aria-hidden="true">'
    + '<div class="modal-dialog" role="document">'
    + '    <div class="modal-content">'
    + '        <div class="modal-header">'
    + '            <button type="button" class="close" data-dismiss="modal" aria-label="Close">'
    + '                <span aria-hidden="true"><img src="'+webz_video_button_close_icon+'" width="15" class="img-fluid" /></span>'
    + '            </button>'
    + '        </div>'
    + '        <div class="modal-body">'
    + '            <iframe src="" frameborder="0"  webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>'
    + '        </div>'
    + '    </div>'
    + '</div>'
    + ' </div>';
    
    $('body').append(modal_html);
    
    var modal_id = '#'+button.modal_id;
    
    if(button.video_url !== ''){
        $(modal_id).on("show.bs.modal", function() {       
            $(this).find("iframe").attr("src", button.video_url);    
        }), $(modal_id).on("hide.bs.modal", function() {
            $(this).find("iframe").attr("src", "");
            $(document).find('.webz-video-button-modal-backdrop').hide();
        });
    }
    else{
        $(modal_id).find('.modal-body').html('<div class="alert alert-danger" role="alert"><big><strong>Oooops!</strong> Can\'t recognize video source.</big></div>');
    }
    
});

})(jQuery);