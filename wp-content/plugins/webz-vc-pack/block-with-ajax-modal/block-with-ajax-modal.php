<?php
$webz_block_with_ajax_modal_init = false;

function webz_block_with_modal_posts_dropdown()
{
    $args = array(
        'post_type' => 'ambassador',
        'post_status' => 'publish',
        'numberposts' => -1
    );

    $posts =  get_posts( $args );
    
    $dropdown = [];
    foreach( $posts as $_post ) {    
        $dropdown[ $_post -> post_title ] = $_post -> ID;  
    }
    return $dropdown;
}

vc_map( array(
    'name' => __( 'Block with ambasador modal on click', 'webz-vc-block-with-ajax-modal' ),
    'base' => 'webz_block_with_ajax_modal',
    'content_element' => true,
    'category' => __( 'WEBZ' ), 
    'icon' => plugin_dir_url(__FILE__) . 'assets/img/icon.png',
    'is_container' => true,
    'as_parent' => array( 'only' => 'vc_column_text, vc_single_image'),
    'params' => array(
        array(
            'type' => 'dropdown',
            'heading' =>  'Ambasador',
            'param_name' => 'post_id',
            'value'       => webz_block_with_modal_posts_dropdown(),
            'description' => __('Ambasador','webz-vc-pack'),
            'admin_label' => true,
        ),
        array(
            'type' => 'dropdown',
            'heading' =>  'Modal size',
            'param_name' => 'size',
            'value'       => array(
                __( 'Normal', 'webz-vc-block-with-ajax-modal' ) => '',
                __( 'Large', 'webz-vc-block-with-ajax-modal' ) => 'modal-lg',
                __( 'Small', 'webz-vc-block-with-ajax-modal' ) => 'modal-sm'
            ),
            'description' => __( 'Modal Size', 'webz-vc-block-with-ajax-modal' ),
            'admin_label' => true
        ),
//        array(
//            'type' => 'textfield',
//            'heading' => __( 'Custom CSS class', 'webz-vc-block-with-ajax-modal' ),
//            'param_name' => 'custom_class',
//            'value' => '',
//            'description' => __( 'Custom CSS class' )
//        ),
        array(
            'type' => 'css_editor',
            'heading' => __( 'Css' ),
            'param_name' => 'css',
            'group' => __( 'Design options', 'webz-vc-block-with-ajax-modal' ),
        ),        
    ),
    'js_view' => 'VcColumnView'
) );

function webz_block_with_ajax_modal_output( $atts, $content )
{   
    global $webz_block_with_ajax_modal_init;
    
    //wp_enqueue_script( 'webz-block-with-ajax-modal' , plugin_dir_url( __FILE__ ) . 'assets/js/script.js' , array( 'bootstrap' ) );
    wp_enqueue_style( 'webz-block-with-ajax-modal' , plugin_dir_url( __FILE__ ) . 'assets/css/style.css', array( 'css_bootstrap' ) );
    
    webz_enqueue_bootstrap();
    
    $js = $css = '';
    
//    if( !$webz_block_with_ajax_modal_init ) {
//        $js .= '<script>var webz_block_with_ajax_modal = [];</script>';
//    }
    
    extract(
        shortcode_atts(
            array(
                'size' => '',
                'post_id' => '',
               // 'custom_class' => '',
                'css' => ''
            ), 
            $atts
        )
    );
        
    $id = webz_generate_unique_id( 'webz-block-with-ajax-modal' );
    
//    $js .= <<<JS
//    <script>
//        webz_block_with_ajax_modal.push({
//            id: '$id',
//            post_id: '$post_id'
//        });
//    </script>
//JS;
    
    $post = get_post( $post_id );
    
    if( get_post_meta( $post -> ID, '_wpb_vc_js_status', true ) ) {
        $vc_custom_post_css = get_field( '_wpb_shortcodes_custom_css', $post -> ID );
        $css .= '<style>' . $vc_custom_post_css . '</style>';
    }
            
    $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ) );
    
    $output = $js . $css . '<a href="' . get_permalink( $post -> ID ) . '" class="webz-block-with-modal vc-ambassador" data-toggle="modal" data-target="#modal-' . $id. '" id="'.$id.'" class="' . $css_class . '">' . do_shortcode( $content ) . '</a>';
    
    $modal = '<div class="modal modal-webz-block-with-modal fade" id="modal-' . $id . '" tabindex="-1" role="dialog" aria-hidden="true">'
            . '     <div class="modal-dialog ' . $size . '">'
            . '         <button type="button" class="close" data-dismiss="modal" aria-label="Close">'
            . '             <span aria-hidden="true">&times;</span>'
            . '         </button>'
            . '         <div class="modal-content">' . apply_filters( 'the_content', $post -> post_content ) . '</div>'
            . '     </div>'
            . '</div>';
    
    add_action( 'wp_footer', function() use ( $modal ) {
        echo $modal;
    } );
    
    $webz_block_with_ajax_modal_init = true;
    
    return $output;    
} 

add_shortcode( 'webz_block_with_ajax_modal', 'webz_block_with_ajax_modal_output' );

add_action( 'admin_enqueue_scripts', function() {
    wp_enqueue_script( 'block-with-ajax-admin', plugin_dir_url( __FILE__ ) . 'assets/js/admin-script.js' , array( 'jquery' ) );
} );

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_Webz_Block_With_Ajax_Modal extends WPBakeryShortCodesContainer{}
}