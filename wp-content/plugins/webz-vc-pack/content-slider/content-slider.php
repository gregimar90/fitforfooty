<?php

vc_map(
    array(
        'name' => 'Content Slider',
        'base' => 'webz_content_slider',
        'as_parent' => array( 'only' => 'webz_content_slider_item' ),
        'description' => __( 'Content slider', 'webz-vc-content-slider' ), 
        'category' => __( 'WEBZ' ),   
        'is_container' => true,
        'content_element' => true,
        'icon' => plugin_dir_url( __FILE__ ) . 'assets/img/icon.png',
        'params' => array(               
            array(
                'type' => 'checkbox',
                'heading' => __( 'Loop' ),
                'param_name' => 'loop',
                'value' => array( ' ' => true),
                'description' => __( 'Display items in loop', 'webz-vc-pack' ),
                'std' => true,
            ),
            array(
                'type' => 'checkbox',
                'heading' => __( 'Navigation', 'webz-vc-pack' ),
                'param_name' => 'nav',
                'value' => array( ' ' => true),
                'description' => __( 'Display navigation buttons', 'webz-vc-pack' ),
                'std' => false,
            ),
            array(
                'type' => 'textfield',
                'heading' => __( 'Previous item text', 'webz-vc-pack' ), 
                'param_name' => 'prev_text',
                'description' => __( 'Text displayed on previous button' ),
                'std' => 'Prev',
                'dependency' => array(
                    'element' => 'nav',
                    'not_empty' => true
                ),
            ),
            array(
                'type' => 'textfield',
                'heading' => __( 'Next item text', 'webz-vc-pack' ), 
                'param_name' => 'next_text',
                'description' => __( 'Text displayed on next button'),
                'std' => 'Prev',
                'dependency' => array(
                    'element' => 'nav',
                    'not_empty' => true
                ),
            ),
            array(
                'type' => 'checkbox',
                'heading' => __( 'Autoplay' ),
                'param_name' => 'autoplay',
                'value' => array( ' ' => true),
                'description' => __( 'Autoplay', 'webz-vc-pack' ),
                'std' => true,
            ),
            array(
                'type' => 'textfield',
                'heading' => __( 'Autoplay timeout', 'webz-vc-pack' ),
                'param_name' => 'autoplay_timeout',
                'value' => 5000,
                'description' => __( 'Autoplay timeout [s]'),
                'dependency' => array(
                    'element' => 'autoplay',
                    'not_empty' => true
                ),
                'std' => '5'
            ),
            array(
                'type' => 'checkbox',
                'heading' => __( 'Autoplay pause on hover', 'webz-vc-pack' ),
                'param_name' => 'autoplay_pause_hover',
                'value' => array( ' ' => true),
                'description' => __( 'Autoplay pause on hover', 'webz-vc-pack' ),
                'std' => true,
                'dependency' => array(
                    'element' => 'autoplay',
                    'not_empty' => true
                ),
            ),
            array(
                'type' => 'textfield',
                'heading' => __( 'Custom class', 'webz-vc-pack' ),
                'param_name' => 'custom_class',
                'value' => '',
                'description' => __( 'Slider Custom Class')
            ),
            array(
                'type' => 'textfield',
                'heading' => __( 'Items per page (extra small screen)', 'webz-vc-pack' ), 
                'param_name' => 'items_per_page_xsmall',
                'description' => __( 'Items per page on extra small screen'),
                'value' => 1,
                'group' => __( 'Design options', 'webz-vc-pack' ),
            ),
            array(
                'type' => 'textfield',
                'heading' => __( 'Items per page (small screen)', 'webz-vc-pack' ), 
                'param_name' => 'items_per_page_small',
                'description' => __( 'Items per page on small screen'),
                'value' => 1,
                'group' => __( 'Design options', 'webz-vc-pack' ),
            ),
            array(
                'type' => 'textfield',
                'heading' => __( 'Items per page (medium screen)', 'webz-vc-pack' ), 
                'param_name' => 'items_per_page_medium',
                'description' => __( 'Items per page on medium screen'),
                'value' => 1,
                'group' => __( 'Design options', 'webz-vc-pack' ),
            ),
            array(
                'type' => 'textfield',
                'heading' => __( 'Items per page (large screen)', 'webz-vc-pack' ), 
                'param_name' => 'items_per_page_large',
                'description' => __( 'Items per page on large screen'),
                'value' => 1,
                'group' => __( 'Design options', 'webz-vc-pack' ),
            ),
            array(
                'type' => 'css_editor',
                'heading' => __( 'Css' ),
                'param_name' => 'css',
                'group' => __( 'Design options', 'webz-vc-pack' ),
            )
        ),
        'js_view' => 'VcColumnView'
    )
);   

vc_map( array(
    'name' => __( 'Content Slider Item', 'webz-vc-content-slider' ),
    'base' => 'webz_content_slider_item',
    'content_element' => true,
    'category' => __( 'WEBZ' ), 
    'icon' => plugin_dir_url(__FILE__) . 'assets/img/icon2.png',
    'is_container' => true,
    'as_child' => array( 'only' => 'webz_content_slider' ),
    'params' => array(
        array(
            'type' => 'css_editor',
            'heading' => __( 'Css' ),
            'param_name' => 'css',
            'group' => __( 'Design options', 'webz-vc-content-slider' ),
        ),        
    ),
    'js_view' => 'VcColumnView'
) );

function webz_content_slider_output( $atts, $content )
{   
    wp_enqueue_script( 'owl-carousel' , plugin_dir_url( __FILE__ ) . '../assets/libs/owl-carousel/dist/owl.carousel.min.js' , array( 'jquery' ) );
    wp_enqueue_script( 'webz-content-slider-js' , plugin_dir_url( __FILE__ ) . 'assets/js/script.js' , array( 'owl-carousel' ) );
    wp_enqueue_style( 'owl-carousel-css' , plugin_dir_url( __FILE__ ) . '../assets/libs/owl-carousel/dist/assets/owl.carousel.css' );
    wp_enqueue_style( 'owl-carousel-theme-css' , plugin_dir_url( __FILE__ ) . '../assets/libs/owl-carousel/dist/assets/owl.theme.default.min.css' );
    
    webz_enqueue_bootstrap();
    
    extract(
        shortcode_atts(
            array(
                'nav' => '',
                'autoplay' => '',
                'loop' => '',
                'autoplay_timeout' => '',
                'autoplay_pause_hover' => '',
                'items_per_page_xsmall' => '',
                'items_per_page_small' => '',
                'items_per_page_medium' => '',
                'items_per_page_large' => '',
                'custom_class' => '',
                'prev_text' => '',
                'next_text' => '',
                'css' => ''
            ), 
            $atts
        )
    );
        
    $slider_id = webz_generate_unique_id( 'content-slider' );
    
    $items_per_page_xsmall = $items_per_page_xsmall ? $items_per_page_xsmall : 1;
    $items_per_page_small = $items_per_page_small ? $items_per_page_small : 1;
    $items_per_page_medium = $items_per_page_medium ? $items_per_page_medium : 1;
    $items_per_page_large = $items_per_page_large ? $items_per_page_large : 1;
    $loop = $loop ? 'true' : 'false';
    $nav = $nav ? 'true' : 'false';
    $prev_text = $prev_text ? esc_html( $prev_text ) : 'Prev';
    $next_text = $next_text ? esc_html( $next_text ) : 'Next';
    $autoplay = $autoplay ? 'true' : 'false';
    $autoplay_timeout = $autoplay_timeout ? ( $autoplay_timeout * 1000 ) : 5000;
    $autoplay_pause_hover  = $autoplay_pause_hover ? 'true' : 'false';
    
    $js = <<<JS
    <script>
       
        if(typeof(webz_content_sliders) === 'undefined' ){var webz_content_sliders = [];}
            
        webz_content_sliders.push({
            id: '$slider_id',
            loop: $loop,
            nav: $nav,
            autoplay: $autoplay,
            autoplayTimeout: $autoplay_timeout,
            autoplayHoverPause: $autoplay_pause_hover,
            items_per_page_xsmall: $items_per_page_xsmall,
            items_per_page_small: $items_per_page_small,
            items_per_page_medium: $items_per_page_medium,
            items_per_page_large: $items_per_page_large,
            prev_text: '$prev_text',
            next_text: '$next_text'
        });
    </script>
JS;
            
    $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ) );
    
    $output = $js . '<div id="'.$slider_id.'" class="owl-carousel owl-theme webz-content-slider' . $css_class . '">' . do_shortcode( $content ) . '</div>';
    
    return $output;    
} 

function webz_content_slider_item_output( $atts, $content )
{   
    extract(
        shortcode_atts(
            array(
                'css' => ''
            ), 
            $atts
        )
    );    
    
    $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ) );
    
    return '<div class="item webz-content-slider-item ' . $css_class . '">' . do_shortcode( $content ) . '</div>';    
} 

add_shortcode( 'webz_content_slider', 'webz_content_slider_output' );
add_shortcode( 'webz_content_slider_item', 'webz_content_slider_item_output' );

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_Webz_Content_Slider extends WPBakeryShortCodesContainer{}
    class WPBakeryShortCode_Webz_Content_Slider_Item extends WPBakeryShortCodesContainer{}
}