function webz_google_maps_init(){
    
    if(webz_google_maps.length > 0){
        
        var bounds = [],
            map = [],
            infowindow = [];     
    
        jQuery.each(webz_google_maps, function(map_i, _map){

            map[map_i] = new google.maps.Map(document.getElementById(_map.id), {
                zoom: 4,
                center: {lat: 0, lng: 0},
                styles: _map.custom_styles
            });

            infowindow[map_i] = new google.maps.InfoWindow({
                content: ''
            });

            bounds[map_i] = new google.maps.LatLngBounds();                   

            if(typeof(webz_google_maps_markers[_map.id]) !== 'undefined'){

                var marker_options,
                    position,
                    marker = [],
                    marker_size;

                jQuery.each(webz_google_maps_markers[_map.id], function(marker_i, _marker){

                    position = new google.maps.LatLng(parseFloat(_marker.lat), parseFloat(_marker.lng));

                    bounds[map_i].extend(position);

                    marker_options = {
                        position: position,
                        map: map[map_i]
                    };                      

                    if(_marker.image !== null){

                        marker_size = _marker.size ? _marker.size : _map.marker_size;

                        marker_options.icon = {
                            url: _marker.image,
                            scaledSize: new google.maps.Size(marker_size, marker_size)
                        };                            
                    }

                    marker[marker_i] = new google.maps.Marker(marker_options);

                    marker[marker_i].addListener('click', function() {

                        infowindow[map_i].setOptions({
                            content: _marker.content,
                            maxWidth: jQuery(document).find('#' + _map.id).width() - 100
                        });

                        infowindow[map_i].open(map[map_i], marker[marker_i]);
                    });

                });

                map[map_i].fitBounds(bounds[map_i]);
                
                google.maps.event.addListenerOnce(map[map_i], 'idle', function() {               
                    if( webz_google_maps_markers[_map.id].length === 1 ) {
                        map[map_i].setZoom( map[map_i].getZoom() - 6  );
                    }
                });
                
            }

        });
    }
    
}