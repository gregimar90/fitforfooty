<?php

add_image_size( 'webz-news-thumb', 340, 245, array( 'center', 'center', true ) );

vc_map(
    array(
        'name' => "News",
        'base' => 'webz_news',
        'description' => __("News", 'webz-vc-pack'), 
        'category' => __('WEBZ'),   
        'icon' => plugin_dir_url(__FILE__).'assets/img/icon.png',
        'params' => array(         
            
            array(
                "type" => "textfield",
                "heading" => __("Default category ID", 'webz-vc-pack'),
                "param_name" => "default_category_id",
                "value" => '',
                "description" => __("Default Category ID"),
            ),
            
            array(
                "type" => "textfield",
                "heading" => __("Content length", 'webz-vc-pack'),
                "param_name" => "content_length",
                "value" => '100',
                "description" => __("Content length"),
            ),
            
            array(
                "type" => "textfield",
                "heading" => __("Custom class", 'webz-vc-pack'),
                "param_name" => "custom_class",
                "value" => '',
                "description" => __("Custom css class"),
            ),
            
            array(
                'type' => 'css_editor',
                'heading' => __( 'Css'),
                'param_name' => 'css',
                'group' => __( 'Design options', 'webz-vc-pack'),
            )
        )
    )
);   


if( !function_exists( 'get_post_thumbnail_url' ) ) {

    function  get_post_thumbnail_url( $post_id, $size = 'normal') {
        $get_thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ),$size, true );

        return $get_thumb[0]!='' ? $get_thumb[0] : null;
    }
}

function webz_news_output( $atts )
{   
    global $post, $webz_news_category_id, $wp_query;
    
    $ajax = isset( $_GET['ajax'] ) ? true : false;
    
    webz_enqueue_bootstrap();
    
    wp_enqueue_style( 'webz-news', plugin_dir_url(__FILE__). 'assets/css/style.css' );    
    wp_enqueue_script( 'webz-news-mixitup', plugin_dir_url(__FILE__). 'assets/js/script.js', array( 'jquery' ) );

    // Params extraction
    extract(
        shortcode_atts(
            array(
                'custom_class' => '', 
                'content_length' => '',
                'default_category_id' => '',
                'css' => ''
            ), 
            $atts
        )
    );
    
    $categories = get_terms( array(
        'taxonomy' => 'category',
        'hide_empty' => false,
    ) );
    
    $output = '';
    
    $count = $count ? $count : '12';
    $content_length = $content_length ? $content_length : 100;
    
    $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ) );
   
    
    
    if( !$default_category_id && !$webz_news_category_id ) {
        $default_category_id = $categories[0] -> term_id;
    }
    
    elseif( $webz_news_category_id !== NULL ) {
        $default_category_id = $webz_news_category_id; 
    }

    
    if( !$ajax ) {
        
        $output .= '<div class="posts webz-news ' . $custom_class . ' '.$css_class.'">';
        
        $output .= '<div class="filters">'
            . '     <ul class="nav justify-content-center">';
        
        foreach( $categories as $category ) {

            if( $category -> slug !== 'uncategorized') {
            
                $output .= '<li class="nav-item">'
                        . '     <a data-cat-slug="' . $category -> slug . '"class="filter-news filter-category nav-link ' . ( $category -> term_id == $default_category_id ? 'active' : '' ) . '" href="' . get_category_link( $category -> term_id ) . '">' . $category -> name . '</a>'
                        . '</li>';            
            
            }

        }
        
        $output .= '</ul></div>';
        
        $output .= '    <div class="row news-container">';
        
    }
    
    $output .= '<div class="webz-news-loader col-sm-12 text-center" style="display: none; margin-bottom: 20px"><div class="news-loader-spinner"></div><br/><h5>Loading News...</h5></div>';
    
    $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
    
    $args = array(
        'post_type' => 'post',
        //'posts_per_page' => $count,
        //'posts_per_page' => 40,
        'paged' => $paged,
        'cat' => $default_category_id
    );
    
    $loop = new WP_Query( $args );   

    if( $loop -> have_posts() ) :
    
        while ( $loop -> have_posts() ) : $loop -> the_post();    

            $thumbnail_id = get_post_thumbnail_id($post->ID);

            $thumbnail_url = $thumbnail_id 
                    ? get_post_thumbnail_url($post->ID, 'webz-news-thumb' )
                    : webz_image_url( webz_get_option( 'no_image_image' ), 'webz-news-thumb' );

            $url = get_permalink( $post ->ID );

            $_description = webz_custom_field_value( 'short_description' , $post -> ID );
            
            $description = ( strlen( $_description ) > $content_length ) ? substr( $_description, 0, $content_length ) . '...' : $_description;

            $views = webz_custom_field_value( 'views' , $post -> ID );

            if( !$views ) {
                $views = 0;
            }
            
            $output .= '<div class="col-sm-3 webz-news-item category-' . $category -> slug . '" ' . ( $ajax ? ''/*'style="display: none"'*/ : '') . '>
                            <div class="post">
                                <div class="post-photo">
                                        <a href="' . $url . '">
                                                <img src="' . $thumbnail_url . '" class="img-fluid" alt="' . $post -> post_title . '">
                                        </a>
                                </div>
                                <div class="post-content">
                                
                                        <div class="post-date">
                                                ' . get_the_date( ) . '
                                        </div>
                                        <div class="post-title">
                                        <a href="' . $url . '">' . $post -> post_title . '</a>
                                        </div>
                                        <div class="post-intro">
                                        <a href="' . $url . '">
                                                ' . $description . '
                                        </a>
                                        </div>


                                </div>
                                <div class="post-footer">
                                        <div class="post-comments">
                                            <img src="' . webz_template_url() . '/assets/img/icon-comment.svg" width="17" alt="Comment icon"> ' . $post -> comment_count . '
                                        </div>
                                        <div class="post-views">
                                                <img src="' . webz_template_url() . '/assets/img/icon-views.svg" width="19" alt="post views icon"> ' . $views . '
                                        </div>

                                        <div class="post-link">
                                                <a href="' . $url . '"><img src="' . webz_template_url() . '/assets/img/icon-link.svg" width="18" alt="post link icon"></a>
                                        </div>

                                </div>
                            </div>
                        </div>';
        endwhile;    
        
        $output .= '<div class="col-sm-12 webz-news-pagination"><div class="row">';    

        $ajax_pagination_filter = ( !$ajax && !is_category() ) || is_category() && $ajax;
        
        $older_url = get_next_posts_page_link( $loop -> max_num_pages );       
        
        $newer_url = get_previous_posts_page_link();
        
        $output .= '<div class="col-sm-6 text-right">';
        
        if( $newer_url !== webz_cur_page_url() ) {
            $output .= '<a class="btn btn-primary ' . ( $ajax_pagination_filter ? 'filter-news' : '' ) . '" href="' . webz_remove_ajax_from_url( $newer_url ). '">&laquo; Previous page </a>';
        }
        
        $output .= '</div><div class="col-sm-6">';
        
        if( $older_url ) {
            $older_url = str_replace( webz_cur_page_url(), get_category_link( $default_category_id ), $older_url );
            $output .= '<a class="btn btn-primary ' . ( $ajax_pagination_filter ? 'filter-news' : '' ) . '" href="' . webz_remove_ajax_from_url( $older_url ) . '">Next Page &raquo; </a>';
        }

        $output .= '</div>';
        
        $output .= '</div></div></div>';

    wp_reset_query();
    
    else:
        $output .= apply_filters( 'the_content', '<div class="col-sm-12 text-center webz-news-no-news"><h4>There\'s no news :( </h4></div>' );
    endif;
    
    if( !isset( $ajax ) ) {
        $output .= '    </div>';
        $output .= '</div>';
    }   
   
    
    return $output;
} 


function webz_remove_ajax_from_url( $url ) {
    return str_replace( '?ajax=true', '', $url );
}

add_shortcode( 'webz_news', 'webz_news_output');