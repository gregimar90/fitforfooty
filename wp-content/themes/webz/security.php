<?php

class Security
{   
    /**
     *
     * @var string
     */
    private static $admin_httpauth_open_tag = '#WEBZ ADMIN HTTP AUTH BEGIN';
    
    /**
     *
     * @var string
     */
    private static $admin_httpauth_close_tag = '#WEBZ ADMIN HTTP AUTH END';
    
    /**
     *
     * @var string
     */
    private static $admin_theme_editor_disable_open_tag = '#WEBZ ADMIN THEME EDITOR DISABLE BEGIN';
    
    /**
     *
     * @var string
     */
    private static $admin_theme_editor_disable_close_tag = '#WEBZ ADMIN THEME EDITOR DISABLE END';
    
    /**
     *
     * @var string
     */
    private static $admin_theme_editor_enable_open_tag = '#WEBZ ADMIN THEME EDITOR ENABLE BEGIN';
    
    /**
     *
     * @var string
     */
    private static $admin_theme_editor_enable_close_tag = '#WEBZ ADMIN THEME EDITOR ENABLE END';
    
    /**
     *
     * @var string
     */
    private static $wp_config_execution_protection_open_tag = '#WEBZ WP-CONFIG EXECUTION PROTECTION BEGIN';
    
    /**
     *
     * @var string
     */
    private static $wp_config_execution_protection_close_tag = '#WEBZ WP-CONFIG EXECUTION PROTECTION END';
    
    /**
     * Create .htaccess & .htpasswd with http basic auth
     * @param string User Login
     * @param string User Password
     * @return boolean
     */
    public static function enableAdminHttpAuth( $login, $_password )
    {        
        if( !Checkers::isAdminHttpAuth() )
        {
            $dir = Helpers::getHomePath() . 'wp-admin/';

            $htpasswd_path = $dir . '.htpasswd';

            $password = crypt( $_password, base64_encode( $_password ) );

            $htaccess_content = 'AuthType Basic' . PHP_EOL
                . 'AuthName "PROTECTED AREA"' . PHP_EOL
                . 'AuthUserFile '.$htpasswd_path.'' . PHP_EOL
                . 'Require valid-user' . PHP_EOL
                . PHP_EOL
                . '<Files admin-ajax.php>' . PHP_EOL
                . '    Order allow,deny' . PHP_EOL
                . '    Allow from all' . PHP_EOL
                . '    Satisfy any' . PHP_EOL
                . '</Files>';

            $htpasswd_content = $login . ':' . $password . PHP_EOL;            
            
            if( file_put_contents( $htpasswd_path, $htpasswd_content ) !== false ) {
                return Helpers::injectCode( 'wp-admin/.htaccess', self::$admin_httpauth_open_tag, self::$admin_httpauth_close_tag, $htaccess_content, false, Helpers::POSITION_BEGIN );                
            }

            return false;
        }
        return true;
    }
    
    /**
     * Disable Basic Http protection frop wp-admin
     * @return boolean
     */
    public static function disableAdminHttpAuth()
    {
        if( Checkers::isAdminHttpAuth() )
        {
            $htpasswd_path = Helpers::getHomePath() . 'wp-admin/.htpasswd';
            if( file_exists( $htpasswd_path ) ) {
                unlink( $htpasswd_path );
            }
            
            if( Helpers::removeInjectedCode( 'wp-admin/.htaccess', self::$admin_httpauth_open_tag, self::$admin_httpauth_close_tag ) ) {
                return Checkers::isAdminHttpAuth() !== true ? true : false;
            }
                    
            return false;
        }
        
        return true;
    }
    
    /**
     * Disable theme files edit via admin
     * @return boolean
     */
    public static function disableAdminThemeEditor()
    {
        if( !Checkers::isThemeEditorDisabled() ) {    
            Helpers::removeInjectedCode('wp-config.php', self::$admin_theme_editor_enable_open_tag, self::$admin_theme_editor_enable_close_tag);
            
            return Helpers::injectCode( 'wp-config.php' , self::$admin_theme_editor_disable_open_tag, self::$admin_theme_editor_disable_close_tag, "define( 'DISALLOW_FILE_EDIT', true );", true, Helpers::POSITION_BEFORE, '// ** MySQL settings' );            
        }
        return true;
    }
    
    /**
     * Enable theme files edit via admin
     * @param boolean $via_constant (default false) Set true if you want to add constant DISALLOW_FILE_EDIT and set it to false
     * @return boolean
     */
    public static function enableAdminThemeEditor( $via_constant = false )
    {
        if( Checkers::isThemeEditorDisabled() ) {
            Helpers::removeInjectedCode( 'wp-config.php', self::$admin_theme_editor_disable_open_tag, self::$admin_theme_editor_disable_close_tag );

            if($via_constant){
                return Helpers::injectCode( 'wp-config.php' , self::$admin_theme_editor_enable_open_tag, self::$admin_theme_editor_enable_close_tag, "define( 'DISALLOW_FILE_EDIT', false );", true, Helpers::POSITION_BEFORE, '// ** MySQL settings' );            
            }        
        }
        return true;
    }
    
    /**
     * Add directive to .htaccess file and block execute wp-config.php file by user
     * @return boolean
     */
    public static function enableWpConfigExecutionProtection()
    {
        if( !Checkers::isWpConfigExecutionProtection() ) {
            
            $code = '<files wp-config.php>' . PHP_EOL
                . '   order allow,deny' . PHP_EOL
                . '   deny from all' . PHP_EOL
                . '</files>';
            
            return Helpers::injectCode( '.htaccess', self::$wp_config_execution_protection_open_tag, self::$wp_config_execution_protection_close_tag, $code, false, Helpers::POSITION_BEGIN );
        }
        
        return true;
    }
}

/**
 * Class with some informations about wordpress, webserver etc.
 */
class Info
{    
    /**
     *
     * @var string
     */
    public static $newestVersion = null;
    
    /**
     *
     * @var string
     */
    public static $currentVersion = null;
    
    /**
     * Return current wordpress version
     * @global type $wp_version
     * @return string
     */
    public static function version()
    {
        global $wp_version;
        
        self::$currentVersion = $wp_version;
        
        return self::$currentVersion;
    }
    
    /**
     * Return newest wordpress version
     * @return string
     */
    public static function newestVersion() {
        $response = wp_remote_get( 'https://api.wordpress.org/core/version-check/1.7/' );
        $body = json_decode( $response['body'] );        
        
        self::$newestVersion = $body -> offers[0] -> version;
        
        return self::$newestVersion;
    }
    
    /**
     * Return PHP version
     * @return string
     */
    public static function phpVersion() {
        return phpversion();
    }
}

/**
 * 
 */
class Checkers
{   
    /**
     * Check if tables in database have default prefix wp_content
     * @global type $wpdb
     * @return boolean
     */
    public static function isDefaultTablesPrefix() {
        global $wpdb;
        return $wpdb -> prefix == 'wp_' ? true : false;
    }
    
    /**
     * Check if there's up-to-date version of wordpress
     * @return boolean
     */
    public static function isUpToDate()
    {
        $current = Info::version();
        $newest = Info::newestVersion();
        
        return $current == $newest ? true : false;
    }    
    
    /**
     * Check if admin is protected by http-auth
     * @return boolean
     */
    public static function isAdminHttpAuth(){
        $http_code = Helpers::getHttpCode( '/wp-admin/' );        
        return $http_code !== null && $http_code == 401 ? true : false; 
    }
    
    /**
     * Check if user can edit theme files via admin panel
     * @return boolean
     */
    public static function isThemeEditorDisabled(){
        return defined( 'DISALLOW_FILE_EDIT' ) && DISALLOW_FILE_EDIT ? true : false;
    }
    
    /**
     * Check if all updates are disabled via constant in wp-config
     * @return boolean
     */
    public static function isAllUpdatesDisabledViaConstant(){
        return defined( 'AUTOMATIC_UPDATER_DISABLED' ) && AUTOMATIC_UPDATER_DISABLED ? true : false;
    }
    
    /**
     * Check if core updates are disabled via constant in wp-config
     * @return boolean
     */
    public static function isCoreUpdatesEnabledViaConstant(){
        return ( defined( 'WP_AUTO_UPDATE_CORE' ) && WP_AUTO_UPDATE_CORE !== false )  ? true : false;
    }
    
    /**
     * Check if all updates are disabled via filter
     * @return boolean
     */
    public static function isAllUpdatesDisabledViaFilter()
    {        
        $priority = has_filter( 'automatic_updater_disabled', '__return_true' );
        
        if( $priority !== false ) {
            $false_priority = has_filter( 'automatic_updater_disabled', '__return_false' );
            return ( !$false_priority ||  ( $false_priority >= $priority ) ) ? false : true;
        }
        
        return false;
    }
    
    /**
     * Check if core updates are disabled via filter
     * @return boolean
     */
    public static function isCoreUpdatesEnabledViaFilter(){
        
        $priority = has_filter( 'auto_update_core', '__return_true' );
        
        if( $priority !== false ) {
            $false_priority = has_filter( 'auto_update_core', '__return_false' );
            return ( !$false_priority ||  ( $false_priority <= $priority ) ) ? true : false;
        }
        
        return true;
    }
    
    /**
     * Check if wp-config.php file is protected against execution
     * @return boolean
     */
    public static function isWpConfigExecutionProtection()
    {        
        $http_code = Helpers::getHttpCode( '/wp-config.php' );
        
        return $http_code !== null && $http_code == 403 ? true : false; 
    }
}



/**
 * 
 */
class Helpers
{
    const POSITION_BEGIN = 'begin';
    const POSITION_END = 'end';
    const POSITION_AFTER = 'after';
    const POSITION_BEFORE = 'before';

    /**
     * Get http code of specific site URL
     * @param type string Part of site URL after domain
     * @return integer|null
     */
    public static function getHttpCode( $path )
    {
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, get_home_url() . $path );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_exec( $ch );
        
        if ( !curl_errno( $ch ) ) {
            $info = curl_getinfo( $ch );            
            return $info['http_code'];
        }
        
        return null;
    }    
    
    /**
     * Remove code injected in file between specified tags (with them too)
     * @param string file path (relative to home path) 
     * @param string $open_tag Opening tag
     * @param string $close_tag Closing tag
     * @return boolean
     */
    public static function removeInjectedCode( $_path, $open_tag, $close_tag )
    {
        $path = Helpers::getHomePath() . $_path;
        
        $code = file_get_contents( $path );
        
        if( $code !== false )
        {            
            $open_tag_pos = strrpos( $code, $open_tag ) - 1; 
            
            if( $open_tag_pos < 0 ){
                $open_tag_pos = 0;
            }
            
            $close_tag_pos = strrpos( $code, $close_tag); 

            if( $open_tag_pos !== false && $close_tag_pos !== false ){
                return file_put_contents( $path, substr_replace( $code, '', $open_tag_pos, ( $close_tag_pos + strlen( $close_tag ) ) - $open_tag_pos + 1 ) ) !== false ? true : false;
            }
            
            return false;        
        }
        
        return false;        
    }
    
    /**
     * Inject code into file between specified tags and position
     * @param string $_path file path (relative to home path) 
     * @param string $open_tag Opening tag
     * @param string $close_tag Closing tag
     * @param string $_code Injected code
     * @param boolean $is_php_file 
     * @param const|string $position Helper::POSITION_[ END, BEGIN, AFTER, BEFORE] 
     * @param string $ab_code Part of code for which after/before code will be injected
     * @return boolean
     */
    public static function injectCode( $_path, $open_tag, $close_tag, $_code, $is_php_file = true, $position = self::POSITION_END, $ab_code = null )
    {
        $code = PHP_EOL . $open_tag . PHP_EOL . $_code . PHP_EOL . $close_tag . PHP_EOL;
        
        self::removeInjectedCode( $_path, $open_tag, $close_tag );
        
        $path = Helpers::getHomePath() . $_path;
        
        $old_code = file_get_contents( $path );
        
        if( $old_code !== false )
        {        
            if( $position == self::POSITION_BEGIN ) {
                if( $is_php_file )
                {
                    $php_op_pos1 = strpos( $old_code, '<?php' );
                    
                    if ( $php_op_pos1 !== false) 
                    {
                        $_old_code = substr_replace( $old_code, '', $php_op_pos1, 5 );
                        $code_ .= '<?php' . $code;    
                        return file_put_contents($path, $code_ . $_old_code) !== false ? true : false;
                    }
                    else 
                    {
                        $php_op_pos2 = strpos( $old_code, '<?' );
                        if ( $php_op_pos2 !== false) {                    
                            $_old_code = substr_replace( $old_code, '', $php_op_pos2, 2 );
                            $code_ .= '<?php' . $code;    
                            return file_put_contents( $path, $code_ . $_old_code ) !== false ? true : false;
                        }
                    }
                }
                
                return file_put_contents( $path, $code . $old_code ) !== false ? true : false;
            }
            elseif( $position == self::POSITION_END )
            {
                if( $is_php_file )
                {
                    $php_cl_pos1 = strrpos( $old_code, '?>' );
                    $php_cl_pos2 = strpos( $old_code, 'php?>' );
                    
                    if ( $php_cl_pos1 !== false && ( $php_cl_pos1 !== ( $php_cl_pos2 + 3 ) ) ) 
                    {
                        $_old_code = substr_replace( $old_code, '', $php_cl_pos1, $php_cl_pos1 + 2 );
                        $code_ .= $code . '?>';    
                        return file_put_contents( $path, $_old_code . $code_ ) !== false ? true : false;
                    }
                    else 
                    {
                        $php_cl_pos2 = strpos( $old_code, 'php?>' );
                        if ( $php_cl_pos2 !== false ) {                    
                            $_old_code = substr_replace( $old_code, '', $php_cl_pos2, $php_cl_pos2 + 4 );
                            $code_ .= $code . '?>';
                            return file_put_contents( $path, $_old_code . $code_ ) !== false ? true : false;
                        }
                    }
                }
                
                return file_put_contents( $path, $old_code . $code ) !== false ? true : false;
            }
            elseif( $position == self::POSITION_AFTER || $position == self::POSITION_BEFORE )
            {
                $pos = strpos( $old_code, $ab_code );
                
                if( $pos !== false) {
                    
                    $code__ = substr( $old_code, 0, $pos );
                    $__code = substr( $old_code, $pos + strlen( $ab_code ) );
                    
                    if( $position == self::POSITION_AFTER ) {
                        return file_put_contents( $path,  $code__ . $ab_code . $code .$__code ) !== false ? true : false;
                    }
                    elseif( $position == self::POSITION_BEFORE ) {
                        return file_put_contents( $path,  $code__ . $code . $ab_code .$__code ) !== false ? true : false;
                    }
                }
                
                return false;
            }
            
            return false;
            
        }
        
        return false;
    }
    
    /**
     * Return path to home
     * @return string
     */
    public static function getHomePath()
    {
        if ( !function_exists( 'get_home_path' ) ) {
            require_once( dirname(__FILE__) . '/../../../wp-admin/includes/file.php' );
        }
        
        return get_home_path();
    }
}

//var_dump( Security::enableWpConfigExecutionProtection() );

//var_dump( Security::disableAdminHttpAuth() );

//var_dump(Helpers::injectCode('test_replace.php', '/* TEST BEGIN */', '/* TEST END */', '$siemka = "Jestem Grześ Marszał";', true, Helpers::POSITION_BEFORE, '/** Sets up WordPress vars and included files. */' )); exit;

//var_dump(Checkers::isWpConfigExecutionProtection());
//add_filter( 'automatic_updater_disabled', '__return_false', 6);
//add_filter( 'automatic_updater_disabled', '__return_true', 7);
//add_filter( 'auto_update_core', '__return_true', 5 );
//add_filter( 'auto_update_core', '__return_false', 4 );

//var_dump( Checkers::isCoreUpdatesEnabledViaFilter() );  exit;

//var_dump( Checkers::isAllUpdatesDisabledViaConstant() ); exit;

//var_dump( Security::disableAdminThemeEditor( true ) );

//if(isset($_GET['au'])){
//    var_dump(Security::enableAdminHttpAuth('admin', 'Webzadminek69!'));
//    var_dump( Security::disableAdminHttpAuth() );
//}

//add_filter( 'auto_update_plugin', '__return_true' );
//add_filter( 'auto_update_theme', '__return_true' );
//add_filter( 'auto_update_core', '__return_true' );