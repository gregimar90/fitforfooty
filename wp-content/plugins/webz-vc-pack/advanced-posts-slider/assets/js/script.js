(function($) {
    
    if(webz_advanced_posts_sliders.length > 0){
        
        $.each(webz_advanced_posts_sliders, function(i, slider){
            
            var h = 0;
            
            $(slider.selector).owlCarousel({
                loop: slider.loop,
                margin:30,
                nav: slider.nav,
                autoplay: slider.autoplay,
                autoplayTimeout: slider.autoplayTimeout,
                autoplayHoverPause: slider.autoplayHoverPause,
                mouseDrag: true,
                touchDrag: false,
                autoHeight: true,
                responsive:{
                    0:{
                        items:1,
                        mouseDrag: false,
                        touchDrag: true
                    },
                    600:{
                        items:3,
                        mouseDrag: false,
                        touchDrag: true
                    },
                    1000:{
                        items:4
                    }
                },
                onInitialized: function(){
                    
                    $(slider.selector).find('.item').each(function(){
                        if($(this).height() > h){
                            h = $(this).height();
                        }
                    });
                    $(slider.selector).find('.item').css('height',h+'px');
                    
                },
                onTranslate: function(){
                    $(slider.selector).find('.owl-stage-outer').css('height',h+20+'px');
                },
                onResize: function(){
                    $(slider.selector).find('.owl-stage-outer').css('height',h+20+'px');
                }
            });
            
        });
        
    }
    
})(jQuery);