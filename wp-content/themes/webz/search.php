<?php

get_header();

$search_query = new WP_Query( array(
    's' => get_search_query(),
    'post_type' => 'post'
) );

?>
<section class="posts">
    <div class="container">
        <div class="row">            
            <div class="col-12 mb-4">
                <form action="/" method="get">
                    <div class="input-group mb-3">
                        <input type="text" name="s" class="form-control" value="<?php echo get_search_query()?>" placeholder="Search..." required>
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit">Search</button>
                        </div>
                    </div>
                </form>
            </div>            
        </div>
        <div class="row">
            <div class="col-sm-12">
            <?php

            if ( $search_query -> have_posts() ) :
                ?><h2><?php echo _e( 'Search Results for')?> : <?php echo get_query_var('s') ?></h2>
                <div class="row">    
                <?php

                $content_length = 100;
                
                while ( $search_query->have_posts() ) : $search_query->the_post(); 
                
                    $thumbnail_id = get_post_thumbnail_id( $post -> ID );

                    $thumbnail_url = $thumbnail_id 
                            ? get_post_thumbnail_url( $post -> ID, 'webz-news-thumb' )
                            : webz_image_url( webz_get_option( 'no_image_image' ), 'webz-news-thumb' );
                    
                    $_description = webz_custom_field_value( 'short_description' , $post -> ID );
            
                    $description = ( strlen( $_description ) > $content_length ) ? substr( $_description, 0, $content_length ) . '...' : $_description;
                    
                    $link = get_permalink( $post -> ID );
                    
                    $views = webz_custom_field_value( 'views' , $post -> ID );

                    if( !$views ) {
                        $views = 0;
                    }
                    
                    ?>
                
                    <div class="col-sm-3">
                        <div class="post">
                            <div class="post-photo">
                                <a href="<?php echo $link?>">
                                    <img src="<?php echo $thumbnail_url?>" class="img-fluid" alt="<?php echo $post -> post_title?>">
                                </a>
                            </div>
                            <div class="post-content">
                                <div class="post-date">
                                     <?php echo get_the_date()?>
                                </div>
                                <div class="post-title">
                                    <a href="<?php echo $link?>"><?php echo $post -> post_title?></a>
                                </div>
                                <div class="post-intro">
                                    <a href="<?php echo $link?>">
                                        <?php echo $description?>
                                    </a>
                                </div>
                            </div>
                            <div class="post-footer">
                                <div class="post-comments">
                                    <img src="<?php echo webz_template_url() ?>/assets/img/icon-comment.svg" width="17" alt="Comment icon"> <?php echo $post -> comment_count?>
                                </div>
                                <div class="post-views">
                                        <img src="<?php echo webz_template_url() ?>/assets/img/icon-views.svg" width="19" alt="post views icon"> <?php echo $views?>
                                </div>
                                <div class="post-link">
                                        <a href="<?php echo $link?>"><img src="<?php echo webz_template_url() ?>/assets/img/icon-link.svg" width="18" alt="post link icon"></a>
                                </div>

                            </div>
                        </div>
                    </div>
                    <?php
                endwhile;
                ?></div><?php
            else:
            ?>
                    <h2>Nothing Found</h2>
                    <div class="alert alert-warning">
                        <?php echo _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords. ')?>
                    </div>
            <?php endif ?>
            </div>
        </div>
    </div>
</section>

<?php 

get_footer();
            