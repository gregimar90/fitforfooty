<?php

if( !function_exists( 'webz_generate_unique_id' ) ) {

    function webz_generate_unique_id($plugin_name = ''){
        return $plugin_name !='' ? $plugin_name . '_' .rand( 0, 99 ) . rand( 0, 99 ) : rand( 0, 99 ) . rand( 0, 99 );
    }
    
}