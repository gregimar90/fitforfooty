<?php
class exampleVcElement extends WPBakeryShortCode {
     
    // INIT
    function __construct() {
        add_action( 'init', array( $this, 'example_vc_element_mapping' ) );
        add_shortcode( 'example_vc_element', array( $this, 'example_vc_element_html' ) );
    }
     
    // Element Mapping
    public function example_vc_element_mapping() {
         
        //Enqueue scripts
        wp_enqueue_script( 'example-js', get_template_directory_uri() . '/vc/js/example.js', array(), false, true);
        
        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
                return;
        }

        // Map the block with vc_map()
        vc_map( 

            array(
                'name' => __('Example VC element', 'text-domain'),
                'base' => 'example_vc_element',
                'description' => __('Example VC element', 'text-domain'), 
                'category' => __('WEBZ', 'text-domain'),   
                'icon' => get_template_directory_uri().'/vc/img/ico_dialog.png',            
                'params' => array(
                    array(
                        'type' => 'checkbox',
                        'class' => '',
                        'heading' => __( 'Example checkbox input', 'text-domain' ),
                        'param_name' => 'example_checkbox',
                        'value' => __( '1', 'text-domain' ),
                        'description' => __( 'This is example checkbox', 'text-domain' ),
                    )                 

                )
            )
        );                                 
        
    } 
     
     
    // Output
    public function example_vc_element_html( $atts )
    {         
        // Params extraction
        extract(
            shortcode_atts(
                array(
                    'example_checkbox' => ''
                ), 
                $atts
            )
        );
        
        ?><h1>This is example VC element</h1><?
        
        if($example_checkbox){
            ?><h2>With example checkbox checked :)</h2><?
        }
    } 
     
}
 
new exampleVcElement();