<?php
get_header();

//$views = webz_custom_field_value( 'views' , $post -> ID );
//
//if( !$views ) {
//    $views = 0;
//}
//
//$new_views = $views + 1;
//
//update_post_meta( $post -> ID, 'views', $new_views );

?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-12"><?php echo apply_filters( 'the_content', $post -> post_content )?></div>
        </div>   
        <div class="row">
            <div class="col-12 text-center">
                <a href="/ambassadors/" class="btn btn-primary btn-lg">See all ambassadors</a>
            </div>
        </div>
    </div>
</section>
<?php

get_footer();
