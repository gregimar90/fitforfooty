(function($){
    
    if(webz_content_sliders.length > 0){
        $.each(webz_content_sliders, function(i, slider){
            $('#'+slider.id).owlCarousel({
                loop: slider.loop,
                margin:10,
                nav: slider.nav,
                autoplay: slider.autoplay,
                autoplayTimeout: slider.autoplayTimeout,
                autoplayHoverPause: slider.autoplayHoverPause,
                navText: [slider.prev_text, slider.next_text],
                mouseDrag: true,
                touchDrag: false,
                responsive:{
                    0: {
                        items: slider.items_per_page_xsmall,
                        mouseDrag: false,
                        touchDrag: true
                    },
                    576:{
                        items: slider.items_per_page_xsmall,
                        mouseDrag: false,
                        touchDrag: true
                    },
                    768:{
                        items: slider.items_per_page_small,
                        mouseDrag: false,
                        touchDrag: true
                    },
                    922:{
                        items: slider.items_per_page_medium
                    },
                    1200:{
                        items: slider.items_per_page_large
                    }
                }
            });
        });
    }    
    
})(jQuery);