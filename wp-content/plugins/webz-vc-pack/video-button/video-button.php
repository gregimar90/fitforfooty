<?php
vc_map(
    array(
        'name' => "Video Button",
        'base' => 'webz_video_button',
        'description' => __("Open video in modal", 'webz-vc-video-button'), 
        'category' => __('WEBZ'),   
        'icon' => plugin_dir_url(__FILE__).'assets/img/icon.png',
        'params' => array(
            array(
                "type" => "textfield",
                "heading" => __("Button text", 'webz-vc-video-button'), 
                "param_name" => "button_text",
                "description" => __("Text on button"),
                "value" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __("Video URL", 'webz-vc-video-button'), 
                "param_name" => "video_url",
                "description" => __("URL to Video"),
                "value" => ''
            ),
            array(
                'type' => 'dropdown',
                'heading' =>  'Service',
                'param_name' => 'service',
                'value'       => array(
                    'Please select' => '',
                    'Youtube' => 'youtube',
                    'Vimeo' => 'vimeo'
                ),
                'description' => __('Button type','webz-vc-video-button'),
                'admin_label' => true,
            ),
            array(
                'type' => 'dropdown',
                'heading' =>  'Type',
                'param_name' => 'type',
                'value'       => array(
                    'Primary' => 'btn-primary',
                    'Outline Primary' => 'btn-outline-primary',
                    'Secondary' => 'btn-secondary',
                    'Outline Secondary' => 'btn-outline-secondary',
                    'Success' => 'btn-success',
                    'Outline Success' => 'btn-outline-success',
                    'Info' => 'btn-info',
                    'Outline Info' => 'btn-outline-info',
                    'Warning' => 'btn-warning',
                    'Outline Warning' => 'btn-outline-warning',
                    'Danger' => 'btn-danger',
                    'Outline Danger' => 'btn-outline-danger',
                    'Link' => 'btn-link'
                ),
                'description' => __('Button type','webz-vc-video-button'),
                'admin_label' => true,
            ),
            array(
                'type' => 'dropdown',
                'heading' =>  'Size',
                'param_name' => 'size',
                'value'       => array(
                    'Normal' => '',
                    'Small' => 'btn-sm',
                    'Large' => 'btn-lg',
                ),
                'std' => '',
                'description' => __('Button size','webz-vc-video-button'),
                'admin_label' => true,
            ),
            array(
                'type' => 'checkbox',
                'heading' => __( 'Full width', 'webz-vc-video-button'),
                'param_name' => 'full_width',
                'value' => array('Yes' => true),
                'description' => __( 'Full width button', 'webz-vc-video-button'),
                'std' => false
            ),
            array(
                "type" => "textfield",
                "heading" => __("Custom class", 'webz-vc-video-button'), 
                "param_name" => "custom_class",
                "description" => __("Custom class for button"),
                "value" => ''
            ),
            array(
                'type' => 'css_editor',
                'heading' => __( 'Css'),
                'param_name' => 'css',
                'group' => __( 'Design options', 'webz-vc-video-button'),
            )
        )
    )
);   

function webz_video_button_output( $atts )
{   
    webz_enqueue_bootstrap(4);
    
    // Params extraction
    extract(
        shortcode_atts(
            array(
                'button_text' => '',
                'video_url' => '',
                'service' => '',
                'type' => '',
                'size' => '',
                'full_width' => '',
                'custom_class' => '',
                'css' => ''
            ), 
            $atts
        )
    );

    wp_enqueue_script('webz-video-button-js', plugin_dir_url(__FILE__).'assets/js/script.js', array('jquery','bootstrap'));
    wp_enqueue_style('webz-video-button-css', plugin_dir_url(__FILE__). 'assets/css/style.css');

    switch($service){
        case 'youtube':
            $url = parse_url($video_url);
            parse_str($url['query'], $query);
            $video_id = isset($query['v']) ? $query['v'] : null;
            
            if($video_id === null){
                $url = explode('/', $video_url);
                $video_id = end($url);
            }
            
            $video_url = "https://www.youtube.com/embed/$video_id?controls=1&showinfo=0&autoplay=1";
            break;
        case 'vimeo':
            $url = explode('/',$video_url);
            $video_id = end($url);
            $video_url = "https://player.vimeo.com/video/$video_id?autoplay=1&loop=1&autopause=0";
            break;
        default:
            $video_url = '';
    }

    $id = 'webz-video-'.rand(0,100).rand(0,100);
    
    $plugin_url = plugin_dir_url(__FILE__);
    
$js = <<<JS
    <script>
        if(typeof webz_video_buttons === 'undefined'){
            var webz_video_buttons = [];
        }
        
        if(typeof webz_video_button_close_icon === 'undefined'){
            var webz_video_button_close_icon = '$plugin_url/assets/img/delete.svg';
        }
        
        webz_video_buttons.push({modal_id: '$id', 'video_url': '$video_url'});
    </script>
JS;
    
    if(!$type){
        $type = 'btn-primary';
    }
    
    $_link = vc_build_link($link);
    
    $btn_class = "btn $type";
    
    if($full_width){
        $btn_class .= ' btn-block';
    }
    
    if($size){
        $btn_class .= ' '.$size; 
    }
    
    if($custom_class){
        $btn_class .= ' '.$custom_class;
    }
    
    $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ));
    
    $btn_class .= ' webz-video-button ' . $css_class;
    
    return $js . '<button class="' . $btn_class . '" data-toggle="modal" data-target="#'.$id.'">' . $button_text . '</button>';
} 
add_shortcode( 'webz_video_button', 'webz_video_button_output');

add_action( 'vc_backend_editor_render', function(){
    wp_enqueue_script('webz-video-button-editor-js', plugin_dir_url(__FILE__).'assets/js/editor-script.js', array('jquery'));
});