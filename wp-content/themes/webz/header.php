<?php
//Some enqueues...
wp_enqueue_style( 'webz', webz_template_url() . '/assets/css/styles.css', array( 'css_bootstrap' ) );
wp_enqueue_style( 'webz-overwrite', webz_template_url() . '/assets/css/styles-overwrite.css', array( 'webz' ) );
wp_enqueue_script( 'webz', webz_template_url() . '/assets/js/app.js', array( 'jquery' ) );
webz_enqueue_bootstrap();
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <title><?php wp_title(); ?></title>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Webz.net.au">
    <link rel="icon" href="<?php echo webz_image_url( webz_get_option( 'favicon' ) )?>">
    <script>
        var template_url = '<?=webz_template_url()?>';
        var home_url = '<?=get_home_url()?>';
    </script>
    <?php wp_head(); ?>
    <?php echo webz_get_option( 'code_head' ); ?>
</head>

<body <?php body_class();?>>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PNL3D96"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    
    <div id="loader">
        <div class="logoCont">
            <img src="<?php echo webz_image_url( webz_get_option( 'logo' ) )?>" width="200" alt="Fit For Footy"  class="img-fluid" />
            Loading...
        </div>
    </div>
    
    <nav class="site-header navbar navbar-expand-lg fixed-top navbar-light ">
        <div class="container">

            <a class="navbar-brand" href="/">
              <img src="<?php echo webz_image_url( webz_get_option( 'logo' ) )?>" width="120" alt="Fit For Footy"  class="img-fluid">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainNav" aria-controls="mainNav"
              aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

            <div id="mainNav" class="collapse navbar-collapse">
                <ul  class="navbar-nav ml-auto">
                    <?php foreach ( webz_generate_menu_array( TOP_MENU_ID ) as $item ):?>
                    <li class="nav-item">
                        <a <?php if( $item['url'] == webz_cur_page_url() ) {?>class="active"<?php }?> href="<?php echo $item['url']?>"><?php echo $item['title']?></a>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </nav>
    
    <div id="main">
