<?php

vc_map(
    array(
        'name' => 'JS Instafeed',
        'base' => 'webz_js_instafeed',
        'description' => __('Instagram Feed', 'webz-vc-js-content-slider'), 
        'category' => __('WEBZ'),   
        'icon' => plugin_dir_url(__FILE__) . 'assets/img/icon.png',
        'params' => array(     
            array(
                'type' => 'textfield',
                'heading' => __('User ID', 'webz-vc-js-instafeed'), 
                'param_name' => 'user_id',
                'description' => __('Instagram User ID'),
                'value' => ''
            ),
            array(
                'type' => 'textfield',
                'heading' => __('Client ID', 'webz-vc-js-instafeed'), 
                'param_name' => 'client_id',
                'description' => __('Instagram Client ID'),
                'value' => ''
            ),
            array(
                'type' => 'textfield',
                'heading' => __('Access Token', 'webz-vc-js-instafeed'), 
                'param_name' => 'access_token',
                'description' => __('Instagram Access Token'),
                'value' => ''
            ),
            array(
                'type' => 'dropdown',
                'heading' =>  'Number of cols',
                'param_name' => 'cols',
                'value'       => array(
                    '2' => '2',
                    '3' => '3',
                    '4' => '4',
                    '6' => '6',
                    '12' => '12'
                    
                ),
                'std' => '6',
                'description' => __('Number of cols','webz-vc-js-instafeed'),
                'admin_label' => true,
            ),
            array(
                'type' => 'textfield',
                'heading' => __('Number of rows', 'webz-vc-js-instafeed'), 
                'param_name' => 'rows',
                'description' => __('Number of rows'),
                'value' => '2'
            ),            
            
            array(
                'type' => 'css_editor',
                'heading' => __( 'Css'),
                'param_name' => 'css',
                'group' => __( 'Design options', 'webz-vc-js-instafeed'),
            )
        )
    )
);   

function webz_js_instafeed_output( $atts )
{
    extract(
        shortcode_atts(
            array(
                'user_id' => '',
                'client_id' => '',
                'access_token' => '',
                'rows' => '',
                'cols' => '',
                'title' => ''
            ), 
            $atts
        )
    );
    
    $cols = $cols != '' ? $cols : 6;
    $rows = $rows != '' ? $rows : 2;
    
    $col_width = 12 / $cols;
    $limit = $cols * $rows;
    
    webz_enqueue_bootstrap();
    
    wp_enqueue_script( 'webz-instafeed-js', plugin_dir_url(__FILE__) . 'assets/js/script.js', array( 'jquery' ) );
    wp_enqueue_style( 'webz-instafeed-css', plugin_dir_url(__FILE__) . 'assets/css/style.css');
    
    
$html = <<<HTML
<script>
    var webz_js_instafeed_user_id = '$user_id';
    var webz_js_instafeed_client_id = '$client_id';
    var webz_js_instafeed_access_token = '$access_token';
    var webz_js_instafeed_limit = '$limit';
    var webz_js_instafeed_col_width = '$col_width';
</script>

<div class="container-fluid">
<div class="row" id="webz-instafeed">
</div>        
</div>

HTML;
    
    return $html;
}

add_shortcode( 'webz_js_instafeed', 'webz_js_instafeed_output' );