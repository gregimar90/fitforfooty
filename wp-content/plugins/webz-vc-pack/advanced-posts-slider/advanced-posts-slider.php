<?php
$webz_advanced_posts_slider_init = false;

function webz_advanced_posts_slider_dropdown_options()
{
    $ignore = ['page','attachment','revision','nav_menu_item','custom_css','customize_changeset','acf','mb-post-type','mb-taxonomy'];

    $dropdown = [];
    foreach(get_post_types() as $slug){
        if(!in_array($slug, $ignore)){
            $pt = get_post_type_object($slug);
            $dropdown[$pt->labels->name] = $slug;
        }
    }
    return $dropdown;
}

vc_map(
    array(
        'name' => "Posts slider",
        'base' => 'webz_advanced_posts_slider',
        'description' => __("Posts slider", 'webz-vc-pack'), 
        'category' => __('WEBZ'),   
        'icon' => plugin_dir_url(__FILE__).'assets/img/icon.png',
        'params' => array(
            array(
                'type' => 'dropdown',
                'heading' =>  'Post type',
                'param_name' => 'post_type',
                'value'       => webz_advanced_posts_slider_dropdown_options(),
                'description' => __('Post type','webz-vc-pack'),
                'admin_label' => true,
            ),
            array(
                "type" => "textfield",
                "heading" => __("Number of posts", 'webz-vc-pack'), 
                "param_name" => "count",
                "description" => __("Number of posts"),
                "value" => 10
            ),
            array(
                'type' => 'dropdown',
                'heading' =>  'Order by',
                'param_name' => 'order_by',
                'value'       => array(
                    'Date' => 'date',
                    'Modified date' => 'modified',
                    'Random' => 'rand',
                    'Title' => 'title',
                    'Author' => 'author'
                    
                ),
                'std' => 'date',
                'description' => __('Order by','webz-vc-pack'),
                'admin_label' => true,
            ),
            array(
                'type' => 'dropdown',
                'heading' =>  'Order method',
                'param_name' => 'order_method',
                'value'       => array(
                    'Descending' => 'DESC',
                    'Ascending' => 'ASC'                   
                ),
                'std' => 'DESC',
                'description' => __('Order method','webz-vc-pack'),
                'admin_label' => true,
            ),
            array(
                'type' => 'checkbox',
                'heading' => __( 'Open in new tab', 'webz-vc-pack'),
                'param_name' => 'target_blank',
                'value' => array('Yes' => true),
                'description' => __( 'Open post in new web browser tab', 'webz-vc-pack'),
                'std' => false
            ),
            array(
                "type" => "textfield",
                "heading" => __("Custom class", 'webz-vc-pack'),
                "param_name" => "custom_class",
                "value" => '',
                "description" => __("Custom css class"),
            ),
            array(
                "type" => "textfield",
                "heading" => __("Custom item class", 'webz-vc-pack'),
                "param_name" => "custom_item_class",
                "value" => '',
                "description" => __("Custom item css class"),
            ),
            array(
                "type" => "textfield",
                "heading" => __("Custom item inner class", 'webz-vc-pack'),
                "param_name" => "custom_item_inner_class",
                "value" => '',
                "description" => __("Custom item inner css class"),
            ),
            array(
                'type' => 'checkbox',
                'heading' => __( 'Display content'),
                'param_name' => 'display_content',
                'value' => array('Yes' => true),
                'description' => __( 'Display content', 'webz-vc-pack'),
                'std' => true,
                'group' => __( 'Design options', 'webz-vc-pack'),
            ),
            array(
                "type" => "textfield",
                "heading" => __("Content length", 'webz-vc-pack'),
                "param_name" => "content_length",
                "value" => 100,
                "description" => __("Content length"),
                'group' => __( 'Design options', 'webz-vc-pack'),
            ),
            array(
                'type' => 'checkbox',
                'heading' => __( 'Loop'),
                'param_name' => 'loop',
                'value' => array('Yes' => true),
                'description' => __( 'Display items in loop', 'webz-vc-pack'),
                'std' => true,
                'group' => __( 'Design options', 'webz-vc-pack'),
            ),
            array(
                'type' => 'checkbox',
                'heading' => __( 'Featured image'),
                'param_name' => 'featured_image',
                'value' => array('Yes' => true),
                'description' => __( 'Display featured image', 'webz-vc-pack'),
                'std' => true,
                'group' => __( 'Design options', 'webz-vc-pack'),
            ),
            array(
                'type' => 'checkbox',
                'heading' => __( 'Navigation', 'webz-vc-pack'),
                'param_name' => 'nav',
                'value' => array('Yes' => true),
                'description' => __( 'Display navigation buttons', 'webz-vc-pack'),
                'std' => false,
                'group' => __( 'Design options', 'webz-vc-pack'),
            ),
            array(
                'type' => 'checkbox',
                'heading' => __( 'Autoplay'),
                'param_name' => 'autoplay',
                'value' => array('Yes' => true),
                'description' => __( 'Autoplay', 'webz-vc-pack'),
                'std' => true,
                'group' => __( 'Design options', 'webz-vc-pack'),
            ),
            array(
                "type" => "textfield",
                "heading" => __("Autoplay timeout", 'webz-vc-pack'),
                "param_name" => "autoplay_timeout",
                "value" => 5000,
                "description" => __("Autoplay timeout [ms]"),
                'group' => __( 'Design options', 'webz-vc-pack'),
            ),
            array(
                'type' => 'checkbox',
                'heading' => __( 'Autoplay pause on hover', 'webz-vc-pack'),
                'param_name' => 'autoplay_pause_hover',
                'value' => array('Yes' => true),
                'description' => __( 'Autoplay pause on hover', 'webz-vc-pack'),
                'std' => true,
                'group' => __( 'Design options', 'webz-vc-pack'),
            ),
            array(
                'type' => 'checkbox',
                'heading' => __( 'Read more button', 'webz-vc-pack'),
                'param_name' => 'read_more',
                'value' => array('Yes' => true),
                'description' => __( 'Autoplay pause on hover', 'webz-vc-pack'),
                'std' => false,
                'group' => __( 'Design options', 'webz-vc-pack'),
            ),
            array(
                "type" => "textfield",
                "heading" => __("Read more text", 'webz-vc-pack'),
                "param_name" => "read_more_text",
                "value" => '',
                "description" => __("Read more button text"),
                'group' => __( 'Design options', 'webz-vc-pack'),
                'dependency' => array(
                        'element' => 'read_more',
                        'not_empty' => true
                ),
            ),
            array(
                "type" => "textfield",
                "heading" => __("Custom class", 'webz-vc-pack'),
                "param_name" => "read_more_class",
                "value" => '',
                "description" => __("Custom read more button class/classes attribute"),
                'group' => __( 'Design options', 'webz-vc-pack'),
                'dependency' => array(
                        'element' => 'read_more',
                        'not_empty' => true
                ),
            ),
            array(
                'type' => 'css_editor',
                'heading' => __( 'Css'),
                'param_name' => 'css',
                'group' => __( 'Design options', 'webz-vc-pack'),
            )
        )
    )
);   


if( !function_exists( 'get_post_thumbnail_url' ) ) {

    function  get_post_thumbnail_url( $post_id, $size = 'normal')
    {
        $get_thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ),$size, true );

        return $get_thumb[0]!='' 
                ? $get_thumb[0] 
                : null;
    }
}

function webz_advanced_posts_slider_output( $atts )
{   
    global $post;
    
    webz_enqueue_bootstrap();
    
    wp_enqueue_style('webz-owl-carousel', plugin_dir_url(__FILE__). '../assets/libs/owl-carousel/dist/assets/owl.carousel.min.css');
    wp_enqueue_style('webz-owl-carousel-theme', plugin_dir_url(__FILE__). '../assets/libs/owl-carousel/dist/assets/owl.theme.default.min.css');
    wp_enqueue_style('webz-advanced-posts-slider', plugin_dir_url(__FILE__). 'assets/css/style.css');
    
    wp_enqueue_script('webz-owl-carousel', plugin_dir_url(__FILE__). '../assets/libs/owl-carousel/dist/owl.carousel.min.js');
    wp_enqueue_script('webz-advanced-posts-slider', plugin_dir_url(__FILE__) . 'assets/js/script.js', array('jquery','webz-owl-carousel'));

    // Params extraction
    extract(
        shortcode_atts(
            array(
                'post_type' => '',
                'count' => '',
                'nav' => '',
                'loop' => '',
                'custom_class' => '',
                'custom_item_class' => '',
                'custom_item_inner_class' => '',
                'featured_image' => '',
                'css' => '',
                'order_by' => '',
                'order_method' => '',
                'display_content' => '',
                'content_length' => '',
                'autoplay' => '',
                'autoplay_timeout' => '',
                'autoplay_pause_hover' => '',
                'read_more' => '',
                'read_more_text' => '',
                'read_more_class' => '',
                'target_blank' => ''
            ), 
            $atts
        )
    );
    $output = "";
    
    $loop = $loop ? 'true' : 'false';
    $nav = $nav ? 'true' : 'false';
    $autoplay = $autoplay ? 'true' : 'false';
    $autoplay_timeout = $autoplay_timeout ? $autoplay_timeout : 5000;
    $autoplay_pause_hover  = $autoplay_pause_hover ? 'true' : 'false';

    $read_more = $read_more != '' ? true : false;
    
    $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ));
    
    $unique_selector = 'webz_advanced_posts_slider'.rand(0,99).rand(0,99);
    

    $output .= '<script>if(typeof(webz_advanced_posts_sliders)==="undefined"){var webz_advanced_posts_sliders = [];}</script>';

    
    $output .= '<script>'
            . 'webz_advanced_posts_sliders.push({'
            . ' selector: "#'.$unique_selector.'",'
            . ' loop: '.$loop.','
            . ' nav: '.$nav.','
            . ' autoplay: '.$autoplay.','
            . ' autoplayTimeout: '.$autoplay_timeout.','
            . ' autoplayHoverPause: '.$autoplay_pause_hover.''
            .'});'
            . '</script>';
    
    $output .= '<div class="webz-advanced-posts-slider owl-carousel owl-theme ' . $custom_class . ' '.$css_class.'" id="'.$unique_selector.'">';
    

    $args = array(
        'post_type' => $post_type,
        'posts_per_page' => $count,
        'order_by' => $order_by,
        'order' => $order_method
    );
    
    
    
    $loop = new WP_Query( $args );   

    while ( $loop->have_posts() ) : $loop->the_post();       
        
        $permalink = get_permalink( $post->ID ); 
    
        $post -> post_content = strip_shortcodes($post->post_content);
        
        $output .= !$read_more 
                ? '<div class="item" onclick="window.open(\''.$permalink.'\', \''.($target_blank ? '_blank' : '_self').'\')" title="'.$post->post_title.'" '.($target_blank ? 'target="_blank"' : "").'><div style="height: 100%" class="' . $custom_item_inner_class . '">'
                : '<div class="item ' . $custom_item_class . '"><div style="height: 100%" class="' . $custom_item_inner_class . '">';
        
        if($featured_image){
            $thumbnail_id = get_post_thumbnail_id($post->ID);
            if($thumbnail_id){
                $thumbnail = get_post_thumbnail_url($post->ID, 'medium_large');
            }else{
                $thumbnail = plugin_dir_url(__FILE__).'assets/img/no-image.png';
            }
            $output .= '<div class="webz-advanced-posts-slider-image" style="background-image: url('.$thumbnail.')"></div>';
        }
        
        $_title = '<h3>'.$post->post_title.'</h3>';    
        
        $title = apply_filters( 'webz_posts_slider_title', (object)array(
            'html' => $_title,
            'permalink' => $permalink,
            'post_title' => $post -> post_title,
            'custom_class' => $custom_class
        ) );
        
        $output .= '<div class="webz-advanced-posts-slider-content">'.$title;
        
        if($display_content){   
            $post -> post_content = strip_tags( $post->post_content );
            $post_content = (strlen($post->post_content) > $content_length) ? substr($post->post_content, 0, $content_length).'...' : $post->post_content;
            $output .= '<p class="webz-advanced-posts-slider-content">'.$post_content.'</p>';
        }
        
        $_read_more_button = '<a href="'.$permalink.'" class="btn btn-secondary '.$read_more_class.'" '.($target_blank ? 'target="_blank"' : "").'>'.$read_more_text.'</a>';
        
        $read_more_button = apply_filters( 'webz_posts_slider_read_more_button', $_read_more_button, (object)array(
            'permalink' => $permalink,
            'read_more_class' => $read_more_class,
            'read_more_text' => $read_more_text,
            'post_id' => $post -> ID,
            'target_blank' => $target_blank
        ) );
        
        $output .= !$read_more 
                ? '</div></div></div>' 
                : '<div class="webz-advanced-posts-slider-read-more-btn"><div class="col-12 text-center">'.$read_more_button.'</div></div>'
                    . '</div></div></div>';
        

    endwhile;       

    wp_reset_query();
    
    $output .= '</div>';
   
    
    return $output;
} 
add_shortcode( 'webz_advanced_posts_slider', 'webz_advanced_posts_slider_output');