<?php
/**
* Plugin Name: WEBZ Visual Composer Plugins Pack
* Plugin URI: https://www.webz.net.au/webz-vc-pack/
* Description: WEBZ Visual Composer Cool Plugins Pack
* Version: 1.0
* Author: Webz.net.au
* Author URI: https://www.webz.net.au
**/

require 'lib/functions.php';

function webz_vc_pack()
{
    
}

function webz_vc_before_init_actions()
{    
    wp_enqueue_style( 'webz-vc-pack', plugin_dir_url( __FILE__ ).'assets/css/style.css', array( 'js_composer_front' ) );
    
    require_once( plugin_dir_path( __FILE__ ) . '/advanced-posts-slider/advanced-posts-slider.php' );
    require_once( plugin_dir_path( __FILE__ ) . '/bootstrap-button/bootstrap-button.php' );
    require_once( plugin_dir_path( __FILE__ ) . '/video-button/video-button.php' );
    require_once( plugin_dir_path( __FILE__ ) . '/js-instafeed/js-instafeed.php' );
    require_once( plugin_dir_path( __FILE__ ) . '/content-slider/content-slider.php' );
    require_once( plugin_dir_path( __FILE__ ) . '/gallery-slider/gallery-slider.php' );
    require_once( plugin_dir_path( __FILE__ ) . '/qa-accordion/qa-accordion.php' );
    require_once( plugin_dir_path( __FILE__ ) . '/google-map/google-map.php' );
    require_once( plugin_dir_path( __FILE__ ) . '/block-with-ajax-modal/block-with-ajax-modal.php' );
    require_once( plugin_dir_path( __FILE__ ) . '/news/news.php' );
    require_once( plugin_dir_path( __FILE__ ) . '/comments/comments.php' );
    
    vc_set_shortcodes_templates_dir(plugin_dir_path( __FILE__ ) . '/templates' );
    
    webz_vc_custom_fields_types();
    webz_vc_custom_fields();    
}

if( !function_exists( 'webz_enqueue_bootstrap' ) ) { 
    function webz_enqueue_bootstrap($version = 4)
    {
        if( $version == 4 ) {
            wp_enqueue_script( 'tether', plugin_dir_url( __FILE__ ).'assets/libs/bootstrap/js/tether.min.js' );
            wp_enqueue_script( 'bootstrap', plugin_dir_url( __FILE__ ).'assets/libs/bootstrap/js/bootstrap.min.js', array( 'jquery', 'tether' ) );
            wp_enqueue_style( 'css_bootstrap', plugin_dir_url( __FILE__ ).'assets/libs/bootstrap/css/bootstrap.min.css',  array( 'js_composer_front' ) );
        }
    }
}

function webz_vc_custom_fields()
{
    //Section
    vc_add_params( 'vc_section',  array(
        array(
            'type' => 'checkbox',
            'heading' => __( 'Container in section', 'webz-vc-pack' ),
            'param_name' => 'section_container',
            'description' => __( 'If checked section will have centered container', 'webz-vc-pack' ),
            'value' => array( ' ' => 'yes' ),
        ),
        array(
            'type' => 'checkbox',
            'heading' => __( 'Container fluid', 'webz-vc-pack' ),
            'param_name' => 'section_container_fluid',
            'description' => __( 'If checked section will have full-width container', 'webz-vc-pack' ),
            'value' => array( ' '  => 'yes' ),
            'dependency' => array(
                'element' => 'section_container',
                'not_empty' => true
            ),
        )
    ) );
}

function webz_vc_custom_fields_types()
{

}

function webz_enqueue_custom_vc_admin_elements()
{
    wp_enqueue_style( 'bootstrap-vc-elements' , plugin_dir_url( __FILE__ ).'assets/css/bootstrap-elements-to-vc-editor.min.css' );
    wp_enqueue_style( 'webz-content-slider-admin-css' , plugin_dir_url( __FILE__ ).'content-slider/assets/css/admin.css' );
    wp_enqueue_style( 'webz-qa-accordion-admin-css' , plugin_dir_url( __FILE__ ).'qa-accordion/assets/css/admin.css' );
    wp_enqueue_style( 'webz-google-map-admin-css' , plugin_dir_url( __FILE__ ).'google-map/assets/css/admin.css' );
    wp_enqueue_style( 'block-with-ajax-modal-admin-css' , plugin_dir_url( __FILE__ ).'block-with-ajax-modal/assets/css/admin.css', array( 'ui-custom-theme' ) );
}

add_action( 'vc_before_init', 'webz_vc_before_init_actions' );

add_action( 'vc_backend_editor_render', function() {
    webz_enqueue_custom_vc_admin_elements();
});

add_action( 'vc_frontend_editor_render', function() {
    webz_enqueue_boostrap_elements_to_vc_editor();
});

add_action( 'init', 'webz_vc_pack' );