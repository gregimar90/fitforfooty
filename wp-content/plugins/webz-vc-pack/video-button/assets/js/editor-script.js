(function($){

    var video_url_selector = 'div[data-vc-shortcode="webz_video_button"] input[name="video_url"]',
        video_service_selector = 'div[data-vc-shortcode="webz_video_button"] select[name="service"]',
        video_url = null,
        service_input = null;
    
    $(document).on('change',video_url_selector, function(){
        video_url = $(this).val();
        
        service_input = $(document).find(video_service_selector);
        
        if(video_url.indexOf('youtube') !== -1 || video_url.indexOf('youtu.be') !== -1){
            service_input.val('youtube');
        }        
        else if(video_url.indexOf('vimeo') !== -1){
            service_input.val('vimeo');
        }
        else{
            service_input.val('');
        }
        
    });

})(jQuery);