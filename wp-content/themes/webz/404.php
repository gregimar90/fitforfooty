<?php

webz_enqueue_bootstrap();

get_header(); ?>
<section class="404">
    <div class="container" style="height: 70vh">
        <div class="row">
            <div class="col-12 text-center">
                <img class="img-fluid mt20" style="max-width: 200px" src="<?php echo webz_image_url( webz_get_option( 'logo' ) )?>"/><br/><br/>
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-center">
                <h1>Oops! That page can&rsquo;t be found</h1>
            </div>
        </div>
    </div>
</section>
<?php get_footer();