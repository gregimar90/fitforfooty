(function($) {
    
    /**
     * Panel div
     * @type jQuery object
     */
    var panel = $('.webz-theme-settings-panel');
    
    /**
     * Loader div
     * @type jQuery object
     */
    var loader = $('.webz-theme-settings-panel-loader-wrap');
    
    //Actions when document is ready
    $(document).ready(function(){
        
        //Hide loader
        loader.hide();  
        
    });
    
    //Actions on click navigation element
    panel.on('click', '.webz-theme-settings-nav a', function(){
        
        //Show loader
        loader.show();
        
    });
    
    //Action on form submit
    panel.on('click', 'input[type="submit"]', function(e){
        e.preventDefault();
        
        /**
         * Form 
         * @type jQuery object
         */
        var form = $(this).closest('form').eq(0);
        
        //If form is invalid
        if(!form.valid()){            
            //Hide loader
            loader.hide();
        }
        //If form is valid
        else{
            //Show loader
            loader.show();
            //Submit form
            form.submit();
        }
        
    });
    
    /**
     * Media inserter using wp_media_uploader
     * @param {jQuery object} selector
     * @returns undefined
     */
    
    function media_inserter(selector){
          
        /**
         * Is multiple?
         * @type Boolean
         */
        var multiple = selector.data('multiple') === 'yes' ? true : false;
                    
        /**
         * Preview wrapper  
         * @type jQuery object
         */      
        var preview_wrap = selector.siblings('.webz-insert-file-preview-wrap').eq(0);
                    
        /**
         * Input wrapper
         * @type jQuery object
         */
        var inputs_wrap = selector.siblings('div.webz-insert-file-inputs-wrap').eq(0);

        /**
         * Input slug
         * @type String
         */
        var input_slug = selector.data('input-slug');
                    
        /**
         * Set preview spacer
         * @type String
         */
        var preview_spacer = selector.data('preview-spacer');

        //WP media uploader
        var wp_media_uploader = wp.media({
            frame:  'post', 
            state:  'insert', 
            multiple:  multiple
        });

        //Action on press insert button in WP media uploader
        wp_media_uploader.on('insert', function(){
            
            /**
             * Media data
             * @type json
             */
            var data = wp_media_uploader.state().get("selection").toJSON();
            
            //Clear hidden inputs wrapper
            inputs_wrap.empty();
            
            //Clear preview wrapper
            preview_wrap.empty();
            
            //If input is multiple
            if(multiple){
                //Last file index
                var last_file_i = data.length - 1;
                
                //Loop media data
                $.each(data, function(i, file){
                    //Append filename to preview wrapper
                    preview_wrap.append(file.filename);                    
                    //Append hidden input to inputs wrapper
                    inputs_wrap.append('<input type="hidden" name="'+input_slug+'[]" value="'+file.id+'">');
                    
                    //If the element is not last
                    if(i < last_file_i){
                        //Append spacer
                        preview_wrap.append(preview_spacer);
                    }
                });
            }
            //If input is not multiple
            else{
                //Append hidden input to inputs wrapper with first element from WP media data
                inputs_wrap.append('<input type="hidden" name="'+input_slug+'" value="'+data[0].id+'">');
                //Append filename to preview wrapper
                preview_wrap.append(data[0].filename);
            }

        });

        //Open WP media uploader
        wp_media_uploader.open();
        
    }
    
    //Open WP media uploader on click
    $('.webz-insert-file').click(function(){
        media_inserter($(this));
    });
    
    
    /* CHECKBOX LIST - BEGIN */
    
    /**
     * Checkbox fields container
     * @type jQuery object
     */
    var checkboxListFields = $('.webz-checkbox-list-field');   
    
    //If there's checkboxes lists
    if(checkboxListFields.length > 0){        
        //Loop checkboxes lists
        checkboxListFields.each(function(){   
            //If checkbox list is required
            if($(this).data('required') === 'yes'){                
                //And there's no selected
                if($(this).find('input:checkbox:checked').length === 0){
                    //Set first input as required
                    $(this).find('input').eq(0).attr('required','required');
                }
                //If input was change
                $(this).change(function(){                    
                    //And there's no selected
                    if( $(this).find('input:checkbox:checked').length === 0){
                        //Set first input as required
                        $(this).find('input').eq(0).attr('required','required');
                    }
                    else{
                        //Otherwise remove required from all inputs
                        $(this).find('input').removeAttr('required');
                    }
                    
                });
            }
        });
    }
    /* CHECKBOX LIST - END */    
    
})(jQuery);