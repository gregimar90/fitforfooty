<?php
//Start session //
session_start();

//Include config file 
include_once 'config.php';

//Images sizes
add_image_size( 'ambasador-thumb', 600, 360, array( 'center', 'center', true ) );

//Disable Visual Composer frontend editor
if ( function_exists( 'vc_disable_frontend' ) ) {
    vc_disable_frontend();
}

//Enable post thumbnails support
add_theme_support( 'post-thumbnails' );

function add_custom_mimes( $upload_mimes ) {
    $upload_mimes['svg'] = 'image/svg+xml';
    $upload_mimes['svgz'] = 'image/svg+xml';
    return $upload_mimes;
}
add_filter( 'upload_mimes', 'add_custom_mimes', 10, 1 );

//Actions active when DEV mode is true
function dev_actions(){    
    show_admin_bar(false );
}

if(DEV){
    dev_actions();
}


//add_filter( 'style_loader_tag', 'preload_styles', 10, 2 );
//
//function preload_styles( $link, $handle ) {
//    $link = str_replace( "rel='stylesheet'", "rel='preload' as='style' onload=\"this.rel='stylesheet'\"", $link );
//    return $link;
//}

//add_filter('style_loader_src', function( $src, $handle ){    
//    $content = file_get_contents( $src );
//    
//    echo "<style>".$content."</style>";
//    
//    return null;
//});


//function pm_remove_all_styles() {
//    global $wp_styles;
//    $wp_styles->queue = array();
//}
//add_action('wp_print_styles', 'pm_remove_all_styles', 100);



/**
 * Init theme widgets
 */
function webz_theme_widgets_init() {
    
    register_sidebar( array(
        'name'          => 'Footer widget area',
        'id'            => 'webz_footer',
        'before_widget' => '<div id="widget-area-webz-footer">',
        'after_widget'  => '</div>',
        'before_title'  => '',
        'after_title'   => ''
    ) );
    
}
add_action( 'widgets_init', 'webz_theme_widgets_init' );

/**
 * Returning array with 2-levels (children in index subitems )
 * @param int $menu_id
 * @return array with menu items
 */
function webz_generate_menu_array( $menu_id )
{
    $arr = array();
    $items = wp_get_nav_menu_items( $menu_id );
    
    foreach( $items as $k => $item ){
        if( $item -> menu_item_parent == 0 ){
            $arr[ $item -> ID ] = (array)$item;           
        }
        else
        {
            if( !isset( $arr[ $item -> menu_item_parent ]['subitems'] ) ){
                $arr[ $item -> menu_item_parent ]['subitems'] = [];
            }
            $arr[ $item -> menu_item_parent ]['subitems'][ $item -> ID ] = (array)$item;
        }
    }
    
    return $arr;
}

/**
 * Getting custom field gallery assigned to custom post type gallery
 */
function ajax_example()
{
    $response = ['a','b','c'];
    echo json_encode( $response ); exit; 
}
add_action("wp_ajax_ajax_example",'ajax_example');
add_action("wp_ajax_nopriv_ajax_example", "ajax_example");

/**
 * @return string template url
 */
function webz_template_url(){
    return esc_url(get_template_directory_uri() );
}

/**
 * @param string $field_name
 * @param int $post_id
 * @return mixed custom input value
 */
function webz_custom_field_value( $field_name, $post_id )
{
    $get = get_post_custom_values( $field_name, $post_id );
    return $get[0] !== '' ? $get[0] : NULL;
}

/**
 * 
 * @param int $id image id
 * @param string $size - image size 
 * @return string image url or null
 */
function webz_image_url( $id, $size = 'full')
{
    $image = wp_get_attachment_image_src( $id, $size );
    return $image[0]!=='' ? $image[0] : NULL;
}

/**
 * Setting flash session
 * @param string $name Session name
 * @param mixed $content - Session data
 */
function webz_set_flash_session( $name, $content )
{
    if( !isset( $_SESSION['webz_flash'] ) ){
        $_SESSION['webz_flash'] = array();
    }
    $_SESSION['webz_flash'][ $name ] = $content;
}

/**
 * Adding content to existing session, if is array - push element, if string - append
 * @param string $name Flash Session name
 * @param mixed $content  Flash Session content
 */
function webz_add_to_flash_session( $name, $content )
{
    if(is_array( $_SESSION['webz_flash'][ $name ] ) ){
        $_SESSION['webz_flash'][ $name ][] = $content;
    }  
    else{
        $_SESSION['webz_flash'][ $name ] .= $content;
    }
}

/**
 * Getting session by name
 * @param string $name Session name
 * @return mixed Session data
 */
function webz_get_flash_session( $name ){
    return isset( $_SESSION['webz_flash'][ $name ] ) ? $_SESSION['webz_flash'][ $name ] : NULL;
}

/**
 * Getting all flash sessions data
 * @return mixed All flash Sessions data or NULL
 */
function webz_get_flash_sessions(){
    return isset( $_SESSION['webz_flash'] ) ? $_SESSION['webz_flash'] : NULL;
}

/**
 * Check if isset any flash sessions
 * @return bool 
 */
function webz_flash_session_exists( $name = NULL){
    return $name ? ( isset( $_SESSION['webz_flash'][ $name ] ) ? true : false )
        : ( isset( $_SESSION['webz_flash'] ) ? true : false );
}

/**
 * Removing all flash sessions
 */
function webz_remove_flash_sessions(){
    unset( $_SESSION['webz_flash'] );
}

/**
 * Actions called after wordpress load
 */
add_action( 'wp_loaded','webz_after_wp_load' );
function webz_after_wp_load(){
    webz_remove_flash_sessions();
}

/**
 * Geting post thumbnail URL
 * @param int $post_id ID of post
 * @param $size - image size, default normal
 * @return string with post thumbnail URL or NULL
 */
function webz_post_thumbnail_url( $post_id, $size = 'normal')
{
    $get_thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ),$size, true );
    
    return $get_thumb[0]!='' 
            ? $get_thumb[0] 
            : null;
}

/**
 * Breaking stirng by word, character etc., and replace it by specified breaker (default <br/>)
 * @param mixed $word - character/word/sth
 * @param string $string - string to breakline
 * @param type $breaker - character/words/sth which word will be broken
 * @return string
 */
function webz_breakline_by( $word, $string, $breaker = '<br/>')
{
    $output = "";
    $explode = explode( $word, $string );
    $count = count( $explode );
    $i=1;
    foreach( $explode as $line )
    {
        $output .= $line;
        
        if( $i<$count ){
            $output .= $breaker;
        }
        
        $i++;
    }
    
    return $output;
}

/**
 * Print r data in pre tags and optional - exit from script after it
 * @param array/object array $data
 * @param bool $exit exit after print r
 */
function webz_print_r( $data, $exit = true )
{
    echo '<pre>'; print_r( $data ); echo '</pre>';
    if( $exit ){
        exit;
    }
}

//TO DESCRIBE
function webz_generate_rating( $params )
{
    $output = $params['before_html'];
    
    for( $i = 0; $i<$params['rate']; $i++ ){
        $output .= $params['fill_html'];
    }
    
    for( $i = 0; $i< ( $params['max_rate'] - $params['rate'] ); $i++ ){
        $output .= $params['empty_html'];
    }
    
    $output .= $params['after_html'];
    
    return $output;
}

/**
 * Highlight word between specified markers in specified method
 * @param string $string - string with word
 * @param string $marker - ex. [marker ]
 * @param array $params - before, after code
 * @return string with highligted word/words
 */
function webz_highlight_marked_word( $string, $marker, $params )
{
    $word = $between = preg_replace( '/(.*)\\' . $marker . '(.*)\\' . $marker . '(.*)/sm', '\2', $string );
    $_word = $params['before'] . $word . $params['after'];
    return str_replace( $marker . $word . $marker, $_word, $string );
}

/**
 * Getting specified param from url
 * @param string $url Url adres
 * @param string $param Param name
 * @return string|null Param value
 */
function webz_get_param_from_url_string( $url, $param )
{
    $query_str = parse_url( $url, PHP_URL_QUERY);
    parse_str( $query_str, $query_params );
    return isset( $query_params[ $param ] ) ? $query_params[ $param ] : null;
}

/**
 * Get option from database using only text after webz_
 * @param string $option Option name without webz_
 * @return mixed Option webz_$option value
 */
function webz_get_option( $option ){
    return get_option( 'webz_' . $option );
}

/**
 * Generate taxonomies items array in proper order and with depth attribute
 * @param string $taxonomy Taxonomy slug
 * @param array $_options Options for WP get_terms ( https://developer.wordpress.org/reference/classes/wp_term_query/__construct/ )
 * @return array with default WP taxonomy items attributes + depth extra param
 */
function webz_generate_taxonomies_array( $taxonomy, $_options = array() )
{
    //Default options
    $default = array(        
        'hierarchical' => true,
        'hide_empty' => false
    );    
    $options = array_replace_recursive( $default, $_options );    
    $options['taxonomy'] = $taxonomy;
    
    //Remove filters which could interfere with the function (custom order etc.)
    remove_all_filters('get_terms');
    
    //WP get taxonomy items
    $terms = get_terms( $options ) ;
    
    $out = $out_temp = $_out = $out_ = [];

    //Loop taxonomy terms
    foreach( $terms as $term )
    {
        //Default term depth is 0
        $term -> depth = 0;

        //If term has parent
        if( $term -> parent!=0){
            //Term depth is equal parent dept + 1
            $term -> depth = $out_temp[ $term -> parent ] -> depth + 1;
        }

        //Add term to temprorary array
        $out_temp[ $term -> term_id ] = $term;
    }

    //Loop temprorary array
    $first = true;
    foreach( $out_temp as $term_id => $term )
    {
        if( !$first ) {
            $out_temp = $out;
        }
            
        //If term has parent
        if( $term -> parent != 0)
        {        
            //Find parent position in array (NOT INDEX!)
            $parent_position =  array_search( $term -> parent, array_keys( $out_temp ) ) + 1;
            
            //Same as above, but for current term
            $self_position = array_search( $term -> $term_id, array_keys( $out_temp ) ) + 1;

            /* Now we will try move current term after parent */
            
            //First part of array from array begin, to parent 
            $_out = array_slice( $out_temp, 0, $parent_position, true );
            
            //Second part of array from element after self position to end of array
            $out_ = array_slice( $out_temp, $self_position + 1, null, true );

            //Out array is combination of first part, current term and last part
            $out = $_out + array( $term_id => $term ) + $out_;
            
            $first = false;
        }
        //Item hasn't parent
        else{
            //Add them to array in normal way
            $out[ $term_id ] = $term;
        }
    }
    
    //Return coool array!
    return $out;
}

if ( ! function_exists( 'webz_is_ajax' ) ) {

    /**
     * Is_ajax - Returns true when the page is loaded via ajax.
     *
     * @return bool
     */
    function webz_is_ajax() {
        return !isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest';
    }
}

/**
 * Get current page URL
 * @return string URL
 */
function webz_cur_page_url()
{    
    $url =  $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
    
    $url .= $_SERVER["SERVER_PORT"] != '80'
            ? $_SERVER['SERVER_NAME'] . ":" . $_SERVER['SERVER_PORT'] . $_SERVER['REQUEST_URI']
            : $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
    
    return $url;
}

//Theme settings
if(is_admin() )
{
    include 'src/libs/theme-settings/theme-settings.php';
    //new WebzThemeSettings
    new WebzThemeSettings(array(
        'Social Links' => array(

            array(
                'name' => 'Facebook URL',
                'slug' => 'webz_facebook_url',
                'type' => 'url',
                'required' => false
            ),

            array(
                'name' => 'Twitter URL',
                'slug' => 'webz_twitter_url',
                'type' => 'url',
                'required' => false
            ),

            array(
                'name' => 'Youtube URL',
                'slug' => 'webz_youtube_url',
                'type' => 'url',
                'required' => true
            ),

            array(
                'name' => 'Google+ URL',
                'slug' => 'webz_googlep_url',
                'type' => 'url',
                'required' => false
            ),
            /*
            array(
                'name' => 'Img Optimizer API Token',
                'slug' => 'webz_img_optimizer_api_token',
                'type' => 'text',
                'required' => true
            ),*/
            
        ),
        'Desing' => array(
            
            array(
                'name' => 'Logo',
                'slug' => 'webz_logo',
                'type' => 'media_insert',
                'required' => true,
                'options' => array(
                    'preview_spacer'=>', '
                )
            ), 
            
            array(
                'name' => 'Favicon',
                'slug' => 'webz_favicon',
                'type' => 'media_insert',
                'required' => true,
                'options' => array(
                    'preview_spacer'=>', '
                )
            ), 
            
            array(
                'name' => 'No-image image',
                'slug' => 'webz_no_image_image',
                'type' => 'media_insert',
                'required' => true
            )  
            
        ),
        
        'Other' => array( 
            array(
                'name' => 'Code in head (js/css etc.)',
                'slug' => 'webz_code_head',
                'required' => false,
                'type' => 'textarea',
                'options' => array(
                    'placeholder' => 'Put code here...',
                    'rows' => 20,
                    'cols' => 50
                )
            ),   
        )
    
    ) );
}


add_filter( 'webz_posts_slider_title', function( $data ){

    return $data -> custom_class == 'newsSlider' 
            ? $data -> html . '<a href="' . $data -> permalink . '">Read more <i class="far fa-chevron-right"></i></a>'
            : $data -> html;
    
});
