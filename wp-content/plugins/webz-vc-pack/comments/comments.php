<?php

vc_map(
    array(
        'name' => 'WEBZ Comments',
        'base' => 'webz_comments',
        'description' => __('Posts Comments', 'webz-vc-comments'), 
        'category' => __('WEBZ'),   
        'icon' => plugin_dir_url(__FILE__) . 'assets/img/icon.png',
        'params' => array(                 
            array(
                'type' => 'css_editor',
                'heading' => __( 'Css'),
                'param_name' => 'css',
                'group' => __( 'Design options', 'webz-vc-comments'),
            )
        )
    )
);   

add_filter('comment_post_redirect', 'redirect_after_comment');

function redirect_after_comment($location) {
    $newurl = substr($location, 0, strpos($location, "#comment"));
    return $newurl . '?webz-comment=1#comment';    
}


function webz_comments_output( $atts )
{
    global $post, $wp_query, $comments;
    
    extract(
        shortcode_atts(
            array(
                'css' => ''
            ), 
            $atts
        )
    );
    
    $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ) );
    
    ob_start();
    
    $commenter = wp_get_current_commenter();
    $req = get_option( 'require_name_email' );
    $aria_req = ( $req ? " required" : '' );
    
    ?><div id="webz-comments" class="webz-comments <?php echo $css_class?>">
        <div class="text-center">
        <?php echo do_shortcode( '[addtoany]' )?>     
        </div>
        
        <?php
    
    $fields = array(
            
        'author' => '<div class="form-group">
                <label for="author">' 
                    .  __( 'Author', 'domainreference' ) 
                    . ( $req ? '<span class="required">*</span>' : '' ) 
                . '</label>
                <input type="text" name="author" id="author" class="form-control clean-bs" placeholder="your nickname" 
                value="' . esc_attr(  $commenter['comment_author'] ) . '" ' . $aria_req . '>
            </div>',

       'email' => '<div class="form-group">
                <label for="email">' 
                    .  __( 'Email', 'domainreference' ) 
                    . ( $req ? '<span class="required">*</span>' : '' ) 
                . '</label>
                <input type="email" name="email" id="email" class="form-control clean-bs" placeholder="email@example.com" 
                value="' . esc_attr(  $commenter['comment_author_email'] ) . '" ' . $aria_req . '>
            </div>'
        
            
    );
    
    comment_form( array(
        'fields' => apply_filters( 'comment_form_default_fields', $fields ),
        'comment_field' => '<div class="form-group">
            <label for="comment">' . _x( 'Comment', 'noun' ) . '<span class="required">*</span></label>
            <textarea class="form-control clean-bs" id="comment" name="comment" rows="3" required></textarea>
          </div>',
        'class_submit' => 'btn btn-primary',
        'label_submit' => __( 'Add Comment' )
    ) );   
   
    if( isset( $_GET['webz-comment'] ) ) : ?>      
    
    <br/>
    <div class="alert alert-warning" id="webz-comments-added">
        <?php echo __( 'Your comment is awaiting moderation', 'domainreference' )?>
    </div>
    <?php
    endif;
    echo '<br/><br/>';
    
    $comments = get_comments( array( 
        'post_id' => $post -> ID,
        'status' => 'approve'
    ) );
    
    wp_list_comments( array(
        'style' => 'div',
        'reply_text' => null,
    ), $comments);
    
    echo '</div>';
    
    return ob_get_clean();
}

add_shortcode( 'webz_comments', 'webz_comments_output' );