<?php
vc_map(
    array(
        'name' => 'Boostrap button',
        'base' => 'webz_bootstrap_button',
        'description' => __( 'Bootstrap button', 'webz-vc-pack' ), 
        'category' => __( 'WEBZ' ),   
        'icon' => plugin_dir_url(__FILE__) . 'assets/img/icon.png',
        'params' => array(
            array(
                'type' => 'vc_link',
                'heading' => __( 'Link' ),
                'param_name' => 'link',
                'description' => __( 'Button link' ),
                'admin_label' => true
            ),
            array(
                'type' => 'dropdown',
                'heading' =>  'Type',
                'param_name' => 'type',
                'value'       => array(
                    'Primary' => 'btn-primary',
                    'Outline Primary' => 'btn-outline-primary',
                    'Secondary' => 'btn-secondary',
                    'Outline Secondary' => 'btn-outline-secondary',
                    'Success' => 'btn-success',
                    'Outline Success' => 'btn-outline-success',
                    'Info' => 'btn-info',
                    'Outline Info' => 'btn-outline-info',
                    'Warning' => 'btn-warning',
                    'Outline Warning' => 'btn-outline-warning',
                    'Danger' => 'btn-danger',
                    'Outline Danger' => 'btn-outline-danger',
                    'Link' => 'btn-link'
                ),
                'description' => __( 'Button type','webz-vc-pack' ),
                'admin_label' => true,
            ),
            array(
                'type' => 'dropdown',
                'heading' =>  'Size',
                'param_name' => 'size',
                'value'       => array(
                    'Normal' => '',
                    'Small' => 'btn-sm',
                    'Large' => 'btn-lg',
                ),
                'std' => '',
                'description' => __( 'Button size','webz-vc-pack' ),
                'admin_label' => true,
            ),
            array(
                'type' => 'checkbox',
                'heading' => __( 'Full width', 'webz-vc-pack' ),
                'param_name' => 'full_width',
                'value' => array( 'Yes' => true),
                'description' => __( 'Full width button', 'webz-vc-pack' ),
                'std' => false
            ),
            array(
                'type' => 'textfield',
                'heading' => __( 'Custom class', 'webz-vc-pack' ), 
                'param_name' => 'custom_class',
                'description' => __( 'Custom class for button' ),
                'value' => ''
            ),
            array(
                'type' => 'css_editor',
                'heading' => __( 'Css' ),
                'param_name' => 'css',
                'group' => __( 'Design options', 'webz-vc-pack' ),
            )
        )
    )
);   

function webz_bootstrap_button_output( $atts )
{   
    webz_enqueue_bootstrap();
    
    // Params extraction
    extract(
        shortcode_atts(
            array(
                'link' => '',
                'type' => '',
                'size' => '',
                'full_width' => '',
                'custom_class' => '',
                'css' => ''
            ), 
            $atts
        )
    );
    
    if( !$type ) {
        $type = 'btn-primary';
    }
    
    $_link = vc_build_link( $link );
    
    $btn_class = "btn $type";
    
    if( $full_width ) {
        $btn_class .= ' btn-block';
    }
    
    if( $size ) {
        $btn_class .= ' ' . $size; 
    }
    
    if( $custom_class ){
        $btn_class .= ' ' . $custom_class;
    }
    
    $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ) );
    
    $btn_class .= $css_class;
    
    $attributes = $_link[ 'url' ] !='' ? 'href="' . $_link[ 'url' ] . '" target="' . $_link[ 'target' ] . '"' : '';
    
    $tag = $_link[ 'url' ] != '' ? 'a' : 'button'; 
    
    return "<$tag " . $attributes. ' class="' . $btn_class. '">' . $_link[ 'title' ]. "</$tag>";
} 
add_shortcode( 'webz_bootstrap_button', 'webz_bootstrap_button_output' );