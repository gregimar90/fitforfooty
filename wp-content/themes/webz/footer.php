    </div>
    
    <div id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <ul>
                        <li>© Copyright Fit For Footy 2018</li>
                        <?php foreach( webz_generate_menu_array( FOOTER_MENU_ID ) as $item ):?>
                        <li>
                            <a href="<?php echo $item['url']; ?>"><?php echo $item['title']; ?></a>
                        </li>
                        <?php endforeach; ?>
                        <li>
                        <li><a href="https://www.webz.net.au" target="_blank" title="Website Design and WordPress Development Melbourne">Web Design Melbourne</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    
    <?php /*
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-12 text-right">
                <?php dynamic_sidebar('webz_footer')?>
                </div>
            </div>
        </div>
    </footer> */?>
    <?php wp_footer(); ?> 
    <!-- Adobe Din Font -->
    <script>
  (function(d) {
    var config = {
      kitId: 'kxm5lom',
      scriptTimeout: 3000,
      async: true
    },
    h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
  })(document);
</script>
   <!-- ./Adobe Din Font -->
</body>
</html>