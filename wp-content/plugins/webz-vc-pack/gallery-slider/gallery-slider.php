<?php

add_image_size( 'vc-slider-gallery', 500, 500, array( 'center', 'center', true ) );

vc_map(
    array(
        'name' => 'Gallery Slider',
        'base' => 'webz_gallery_slider',
        'description' => __('Gallery Slider', 'webz-vc-content-slider'), 
        'category' => __('WEBZ'),   
        'icon' => plugin_dir_url(__FILE__) . 'assets/img/icon.png',
        'params' => array(     
            array(
                'type' => 'attach_images',
                'heading' => __('Images', 'webz-vc-gallery-slider'), 
                'param_name' => 'images',
                'description' => __('Images from gallery'),
                'value' => '',
                'admin_label' => true
            ),            
            array(
                'type' => 'checkbox',
                'heading' => __( 'Loop'),
                'param_name' => 'loop',
                'value' => array(' ' => true),
                'description' => __( 'Display items in loop', 'webz-vc-pack'),
                'std' => true,
            ),
            array(
                'type' => 'checkbox',
                'heading' => __( 'Preview on click', 'webz-vc-pack'),
                'param_name' => 'preview_on_click',
                'value' => array(' ' => true),
                'description' => __( 'Preview image on click', 'webz-vc-pack'),
                'std' => false,
            ),
            array(
                'type' => 'checkbox',
                'heading' => __( 'Navigation', 'webz-vc-pack'),
                'param_name' => 'nav',
                'value' => array(' ' => true),
                'description' => __( 'Display navigation buttons', 'webz-vc-pack'),
                'std' => false,
            ),
            array(
                "type" => "textfield",
                "heading" => __("Previous item text", 'webz-vc-pack'), 
                "param_name" => "prev_text",
                "description" => __("Text displayed on previous button"),
                'std' => 'Prev',
                'dependency' => array(
                        'element' => 'nav',
                        'not_empty' => true
                ),
            ),
            array(
                "type" => "textfield",
                "heading" => __("Next item text", 'webz-vc-pack'), 
                "param_name" => "next_text",
                "description" => __("Text displayed on next button"),
                'std' => 'Prev',
                'dependency' => array(
                        'element' => 'nav',
                        'not_empty' => true
                ),
            ),
            array(
                'type' => 'checkbox',
                'heading' => __( 'Autoplay'),
                'param_name' => 'autoplay',
                'value' => array(' ' => true),
                'description' => __( 'Autoplay', 'webz-vc-pack'),
                'std' => true,
            ),
            array(
                "type" => "textfield",
                "heading" => __("Autoplay timeout", 'webz-vc-pack'),
                "param_name" => "autoplay_timeout",
                "value" => 5000,
                "description" => __("Autoplay timeout [s]"),
                'dependency' => array(
                        'element' => 'autoplay',
                        'not_empty' => true
                ),
                'std' => '5'
            ),
            array(
                'type' => 'checkbox',
                'heading' => __( 'Autoplay pause on hover', 'webz-vc-pack'),
                'param_name' => 'autoplay_pause_hover',
                'value' => array(' ' => true),
                'description' => __( 'Autoplay pause on hover', 'webz-vc-pack'),
                'std' => true,
                'dependency' => array(
                        'element' => 'autoplay',
                        'not_empty' => true
                ),
            ),
            array(
                "type" => "textfield",
                "heading" => __("Custom class", 'webz-vc-pack'),
                "param_name" => "custom_class",
                "value" => '',
                "description" => __("Slider Custom Class")
            ),
            array(
                "type" => "textfield",
                "heading" => __("Items per page (extra small screen)", 'webz-vc-pack'), 
                "param_name" => "items_per_page_xsmall",
                "description" => __("Items per page on extra small screen"),
                "value" => 1,
                'group' => __( 'Design options', 'webz-vc-pack'),
            ),
            array(
                "type" => "textfield",
                "heading" => __("Items per page (small screen)", 'webz-vc-pack'), 
                "param_name" => "items_per_page_small",
                "description" => __("Items per page on small screen"),
                "value" => 1,
                'group' => __( 'Design options', 'webz-vc-pack'),
            ),
            array(
                "type" => "textfield",
                "heading" => __("Items per page (medium screen)", 'webz-vc-pack'), 
                "param_name" => "items_per_page_medium",
                "description" => __("Items per page on medium screen"),
                "value" => 1,
                'group' => __( 'Design options', 'webz-vc-pack'),
            ),
            array(
                "type" => "textfield",
                "heading" => __("Items per page (large screen)", 'webz-vc-pack'), 
                "param_name" => "items_per_page_large",
                "description" => __("Items per page on large screen"),
                "value" => 1,
                'group' => __( 'Design options', 'webz-vc-pack'),
            ),
            array(
                'type' => 'checkbox',
                'heading' => __( 'No margin', 'webz-vc-pack'),
                'param_name' => 'no_margin',
                'value' => array(' ' => true),
                'description' => __( 'Don\'t display image margin', 'webz-vc-pack'),
                'std' => false,
                'group' => __( 'Design options', 'webz-vc-pack'),
            ),
            array(
                'type' => 'css_editor',
                'heading' => __( 'Css'),
                'param_name' => 'css',
                'group' => __( 'Design options', 'webz-vc-pack'),
            )
        )
    )
);   

function webz_gallery_slider_output( $atts )
{    extract(
        shortcode_atts(
            array(
                'images' => '',
                'preview_on_click' => '',
                'nav' => '',
                'autoplay' => '',
                'loop' => '',
                'autoplay_timeout' => '',
                'autoplay_pause_hover' => '',
                'items_per_page_xsmall' => '',
                'items_per_page_small' => '',
                'items_per_page_medium' => '',
                'items_per_page_large' => '',
                'no_margin' => '',
                'custom_class' => '',
                'prev_text' => '',
                'next_text' => '',
                'css' => ''
            ), 
            $atts
        )
    );

    wp_enqueue_script( 'owl-carousel' , plugin_dir_url(__FILE__).'../assets/libs/owl-carousel/dist/owl.carousel.min.js' , array( 'jquery' ) );
    
    wp_enqueue_script( 'webz-gallery-slider-js' , plugin_dir_url(__FILE__).'assets/js/script.js' , array( 'owl-carousel' ) );
    
    wp_enqueue_style( 'owl-carousel-css' , plugin_dir_url(__FILE__).'../assets/libs/owl-carousel/dist/assets/owl.carousel.css');
    wp_enqueue_style( 'owl-carousel-theme-css' , plugin_dir_url(__FILE__).'../assets/libs/owl-carousel/dist/assets/owl.theme.default.min.css');
    
    if( $preview_on_click )
    {    
        wp_enqueue_script( 'feahterlight-js' , plugin_dir_url(__FILE__).'../assets/libs/featherlight/release/featherlight.min.js' , array( 'jquery' ) );
        wp_enqueue_script( 'feahterlight-gallery-js' , plugin_dir_url(__FILE__).'../assets/libs/featherlight/release/featherlight.gallery.min.js' , array( 'feahterlight-js' ) );    
    
        wp_enqueue_style( 'featherlight-css' , plugin_dir_url(__FILE__).'../assets/libs/featherlight/release/featherlight.min.css');
        wp_enqueue_style( 'featherlight-gallery-css' , plugin_dir_url(__FILE__).'../assets/libs/featherlight/release/featherlight.gallery.min.css');
    }
    
    wp_enqueue_style( 'webz-gallery-slider-css' , plugin_dir_url(__FILE__).'assets/css/style.css' , array( 'owl-carousel-theme-css' ) );
        
    $slider_id = webz_generate_unique_id('content-slider');
    
    $items_per_page_xsmall = $items_per_page_xsmall ? $items_per_page_xsmall : 1;
    $items_per_page_small = $items_per_page_small ? $items_per_page_small : 1;
    $items_per_page_medium = $items_per_page_medium ? $items_per_page_medium : 1;
    $items_per_page_large = $items_per_page_large ? $items_per_page_large : 1;
    $loop = $loop ? 'true' : 'false';
    $nav = $nav ? 'true' : 'false';
    $prev_text = $prev_text ? esc_html($prev_text) : 'Prev';
    $next_text = $next_text ? esc_html($next_text) : 'Next';
    $autoplay = $autoplay ? 'true' : 'false';
    $autoplay_timeout = $autoplay_timeout ? ($autoplay_timeout * 1000) : 5000;
    $autoplay_pause_hover  = $autoplay_pause_hover ? 'true' : 'false';
    
    $js = <<<JS
    <script>
        if(typeof(webz_gallery_sliders) === 'undefined'){
            var webz_gallery_sliders = [];
        }

        webz_gallery_sliders.push({
            id: '$slider_id',
            loop: $loop,
            nav: $nav,
            autoplay: $autoplay,
            autoplayTimeout: $autoplay_timeout,
            autoplayHoverPause: $autoplay_pause_hover,
            items_per_page_xsmall: $items_per_page_xsmall,
            items_per_page_small: $items_per_page_small,
            items_per_page_medium: $items_per_page_medium,
            items_per_page_large: $items_per_page_large,
            prev_text: '$prev_text',
            next_text: '$next_text'
        });
    </script>
JS;
    
    $images = explode(',', $images);
    
    $items_html = '';
    
    $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ));
    
    foreach($images as $image_id)
    {            
        $image_url =  webz_image_url( $image_id,  'vc-slider-gallery' );
        $large_image_url =  webz_image_url( $image_id,  'large' );
        
        $items_html .= '<div class="item"  '.( $preview_on_click ? 'data-featherlight="'.$large_image_url.'"' : "" ) .'><img src="' . $image_url . '" alt=""></div>';
    }
    
    if($no_margin){
        $css_class .= ' no-margin';
    }
    
$html = <<<HTML
        $js
<div class="owl-carousel owl-theme webz-gallery-slider $css_class" id="$slider_id">
    $items_html
</div>          

HTML;
    
    return $html;
}

add_shortcode( 'webz_gallery_slider', 'webz_gallery_slider_output' );