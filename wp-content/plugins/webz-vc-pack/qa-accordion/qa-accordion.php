<?php
vc_map(
    array(
        'name' => 'QA Accordion',
        'base' => 'webz_qa_accordion',
        'as_parent' => array('only' => 'webz_qa_accordion_item'),
        'description' => __('QA Accordion', 'webz-vc-qa-accordion'), 
        'category' => __('WEBZ'),   
        'is_container' => false,
        'content_element' => true,
        'icon' => plugin_dir_url(__FILE__) . 'assets/img/icon.png',
        'params' => array(        
            array(
                "type" => "textfield",
                "heading" => __("Custom class", 'webz-vc-pack'),
                "param_name" => "custom_class",
                "value" => '',
                "description" => __("Accordion Custom Class"),
                'group' => __( 'Design options', 'webz-vc-qa-accordion'),
            ),
            array(
                'type' => 'css_editor',
                'heading' => __( 'Css'),
                'param_name' => 'css',
                'group' => __( 'Design options', 'webz-vc-qa-accordion'),
            )
        ),
        'js_view' => 'VcColumnView'
    )
);   

vc_map( array(
    'name' => __('QA Item', 'webz-vc-qa-accordion'),
    'base' => 'webz_qa_accordion_item',
    'category' => __('WEBZ'), 
    'icon' => plugin_dir_url(__FILE__) . '/assets/img/icon2.png',
    'as_child' => array('only' => 'webz_qa_accordion'),
    'params' => array(
        array(
            'type' => 'textfield',
            'heading' => __('Question', 'webz-vc-qa-accordion'),
            'param_name' => 'question',
            'description' => __('Question', 'webz-vc-qa-accordion'),
            'admin_label' => true,
        ),
        array(
            'type' => 'textarea_html',
            'heading' => __('Answer', 'webz-vc-qa-accordion'),
            'param_name' => 'content',
            'description' => __('Answer', 'webz-vc-qa-accordion'),
        )      
    )
) );

function webz_qa_accordion_output( $atts, $content )
{   
    extract(
        shortcode_atts(
            array(
                'css' => '',
                'custom_class' => ''
            ), 
            $atts
        )
    );  
    
    wp_enqueue_script('webz-qa-accordion-js', plugin_dir_url(__FILE__) . 'assets/js/script.js', array('jquery-ui-accordion'));
    wp_enqueue_style('webz-qa-accordion-css', plugin_dir_url(__FILE__) . 'assets/css/style.css');
    
    $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ));
    
    return '<ul class="webz-qa-accordion '.$css_class.' '.$custom_class.'">'.do_shortcode($content).'</ul>'; 
} 

function webz_qa_accordion_item_output($atts, $content)
{       
    extract(
        shortcode_atts(
            array(
                'question' => '',
            ), 
            $atts
        )
    );    
    
    return '<li>
        <h3>'.$question.'</h3>
        <div>
            '.$content.'
        </div>
    </li>';
} 

add_shortcode( 'webz_qa_accordion', 'webz_qa_accordion_output');
add_shortcode( 'webz_qa_accordion_item', 'webz_qa_accordion_item_output');

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_Webz_Qa_Accordion extends WPBakeryShortCodesContainer{}
}