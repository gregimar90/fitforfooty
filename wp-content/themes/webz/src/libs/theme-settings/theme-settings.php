<?php              
class WebzThemeSettings extends WebzThemeSettingsFields
{    
    /**
     * Fields list with custom params
     * @var array 
     */
    private $fields;
    
    /**
     * Settings section name
     * @var string 
     */
    private $section;
    
    /**
     * Loader Html
     * @var string
     */
    private $loader = '<div class="webz-theme-settings-panel-loader-wrap"><div class="webz-theme-settings-panel-loader-logo"></div><div class="webz-theme-settings-panel-loader-spinner"></div></div>';
        
    /**
     * Generate settings page with custom fields
     * @param array $fields - fields list
     */
    public function __construct($fields)
    {       
        //Set fields
        $this -> fields = $fields;   
        
        //If $_GET witch section name exists
        if(isset($_GET['section'])){
            //Storage section name in session
            $_SESSION['webz_settings_section'] = $_GET['section'];
            
            //And redirect to settings page
            header('Location: admin.php?page=webz-theme-settings');
        }
        //If there's no $_GET section
        else{
            //If there's no section in session
            if(!isset($_SESSION['webz_settings_section'])){
                //Get fields array keys
                $keys = array_keys($this -> fields);
                //And set first section from array as active
                $_SESSION['webz_settings_section'] = $keys[0];
            }
        }
        
        //Set section from session as active
        $this -> section = $_SESSION['webz_settings_section'];
        
        //Add settings page
        $this -> addSettingsPage();
        
        //Generate fields
        $this -> generateFields();
        
    }
    
    /**
     * Generate settings page with custom sections and inputs
     */
    private function themeSettingsPage()
    {
        //Enqueue WP Media inserter
        wp_enqueue_media(); 
        
        //Enqueue theme settings page css
        wp_enqueue_style('webz-theme-settings', webz_template_url().'/src/libs/theme-settings/assets/css/webz-theme-settings.css');
        
        //Enqueue jquery validation plugin script
        wp_enqueue_script('webz-jquery-validation', webz_template_url().'/assets/libs/jquery-validation/jquery.validate.min.js', array('jquery'), '', true );
        wp_enqueue_script('webz-jquery-validation-additional-methods', webz_template_url().'/assets/libs/jquery-validation/additional-methods.min.js', array('webz-jquery-validation'), '', true );
        
        //Enqueue theme settings page js
        wp_enqueue_script('webz-theme-settings', webz_template_url().'/src/libs/theme-settings/assets/js/webz-theme-settings.js', array('jquery'), '', true );

        //Output html
        ?>
        <div class="wrap">
            <h1><img src="<?=webz_template_url()?>/src/libs/theme-settings/assets/img/webz_logo.svg" height="25"/> Settings Panel</h1>
            <?php settings_errors();?>
            <div class="welcome-panel webz-theme-settings-panel">
                <?=$this->loader?>
                <div class="webz-theme-settings-nav">
                <?foreach(array_keys($this->fields) as $section):?>
                <a href="admin.php?page=webz-theme-settings&section=<?=$section?>" class="button button-secondary<?if($section==$this->section){?> active<?}?>"><?=$section?></a> 
                <?endforeach?>
                </div>
                <form method="post" action="options.php">
                    <?php 
                        //Display submit button - panel header
                        ?><div class="webz-theme-settings-submit-button-wrap webz-theme-settings-submit-button-wrap-header"><?
                        submit_button('Save Settings', 'large', '', false); 
                        ?></div><?
                        
                        //Output nonce, action, and option_page fields for a settings page.
                        settings_fields($this -> section); 
                        
                        //Prints out all settings sections added to a particular settings page
                        do_settings_sections("webz-theme-options");  
                        
                        //Display submit button - panel footer
                        ?><div class="webz-theme-settings-submit-button-wrap webz-theme-settings-submit-button-wrap-footer"><?
                        submit_button('Save Settings', 'large', '', false); 
                        ?></div><?
                    ?>          
                </form>
            </div>
        </div>
        <?php
    }
    
    /**
     * Adding settings page to admin menu and generate html
     */
    private function addSettingsPage()
    {
        //Add menu element
        add_action("admin_menu", function(){
            //Add page
            add_menu_page("Webz Settings", "Webz Settings", "manage_options", "webz-theme-settings", function(){$this -> themeSettingsPage();}, webz_template_url().'/src/libs/theme-settings/assets/img/webz_icon16.png' , 99);
        });
    }
    
    /**
     * Generate fields html
     */
    private function generateFields()
    {
        //On WP admin init
        add_action("admin_init", function(){
            
            //Set section fields geted from specified section 
            $section = $this -> fields[$this->section];

            /**
             * WP add settings section
             * string $id Slug-name to identify the section. Used in the 'id' attribute of tags.             
             * string $title Formatted title of the section. Shown as the heading for the section.             
             * callable $callback Function that echos out any content at the top of the section (between heading and fields).             
             * string $page The slug-name of the settings page on which to show the section. Built-in pages include 'general', 'reading', 'writing', 'discussion', 'media', etc. Create your own using
             */
            add_settings_section($this -> section, $section['name'], null, "webz-theme-options");

            //Loop section fields
            foreach($section as $field)
            {                   
                //If there's no required param - set false as default
                if(!isset($field['required'])){
                    $field['required'] = false;
                }
                
                //If field is required
                if($field['required']){
                    //Append red star to field name
                    $field['name'] .= '<span style="color: red; font-weight: bold"> *</span>';
                }
                
                /**
                 * WP Add settings field
                 * string $id Slug-name to identify the field. Used in the 'id' attribute of tags.                 
                 * string $title Formatted title of the field. Shown as the label for the field during output.
                 * callable $callback Function that fills the field with the desired form inputs. The function should echo its output.                 
                 * string $page The slug-name of the settings page on which to show the section (general, reading, writing, ...).
                 * string $section Optional. The slug-name of the section of the settings page in which to show the box. Default 'default'.                 
                 * array $args Optional. Extra arguments used when outputting the field.
                 */
                add_settings_field($field['slug'], $field['name'], function($field){                        

                    switch($field['type'])
                    {
                        //Url field
                        case 'url':
                            self::urlField($field['slug'], $field['required'], $field['options']);
                            break;
                        
                        //Text field
                        case 'text':
                            self::textField($field['slug'], $field['required'], $field['options']);
                            break;
                        
                        //Textarea field
                        case 'textarea':
                            self::textareaField($field['slug'], $field['required'], $field['options']);
                            break;
                        
                        //Media insert field
                        case 'media_insert':
                            self::mediaInsertField($field['slug'], $field['required'], $field['options']);
                            break;
                        
                        //Checkbox field
                        case 'checkbox':
                            self::checkboxField($field['slug'], $field['required'], $field['options']);
                            break;
                        
                        //Checkbox list field
                        case 'checkbox_list':
                            self::checkboxListField($field['slug'], $field['required'], $field['items'], $field['options']);
                            break;
                        
                            //Checkbox list field - if someone will make mistake and add "s" after checkbox word
                            case 'checkboxs_list':
                                self::checkboxListField($field['slug'], $field['required'], $field['items'], $field['options']);
                                break;
                        
                        //Dropdown field
                        case 'dropdown':
                            self::dropdownField($field['slug'], $field['required'], $field['items'], $field['options']);
                            break;
                        
                        //Posts picker
                        case 'posts_picker':
                            self::postsPickerField($field['slug'], $field['posts_type'], $field['required'], $field['options']);
                            break;
                        
                            //Posts picker - if someone will make mistake and forget "s" after post word
                            case 'post_picker':
                                self::postsPickerField($field['slug'], $field['posts_type'], $field['required'], $field['options']);
                                break;
                            
                        //Post types picker
                        case 'post_type_picker':
                            self::postTypePickerField($field['slug'], $field['required'], $field['options']);
                            break;
                        
                            //Posts picker - if someone will make mistake and add "s" after type word
                            case 'post_types_picker':
                                self::postTypePickerField($field['slug'], $field['required'], $field['options']);
                                break;
                            
                        //Taxonomy items picker
                        case 'taxonomy_items_picker':
                            self::taxonomyItemsPickerField($field['slug'], $field['taxonomy'], $field['required'], $field['options']);
                            break;
   
                    }

                } , 'webz-theme-options', $this -> section, $field);
                
                /**
                 * string $option_group A settings group name. Should correspond to a whitelisted option key name. Default whitelisted option key names include "general," "discussion," and "reading," among others. 
                 * string $option_name The name of an option to sanitize and save.
                 */
                register_setting($this -> section, $field['slug']);
            }
            
            
        });
    }  

}

/**
 * Abstract class generating specified field html
 */
abstract class WebzThemeSettingsFields
{
    /**
     * Text field
     * @param string $name Field slug
     * @param bool $required Optional. Default false  
     * @param array $_options Field options:
     *  - placeholder string Input placeholder. Default empty
     */
    protected static function textField($name, $required = false, $_options = [])
    {
        $default = array(
            'placeholder' => ''
        );
        
        $options = wp_parse_args( $_options, $default );
        
        //Output
        echo '<input placeholder="'.$options['placeholder'].'" type="text" '.($required ? 'required="required"' : '').' name="'.$name.'" value="'.get_option($name).'" />';
    }    
    
    /**
     * Textarea field
     * @param string $name Field slug
     * @param bool $required Optional. Default false  
     * @param array $_options Field options:
     *  - rows integer Input height. Default 8
     *  - placeholder string Textarea placeholder. Default empty
     */
    protected static function textareaField($name, $required = false, $_options = [])
    {
        $default = array(
            'rows' => 8,
            'placeholder' => ''
        );
        
        $options = wp_parse_args( $_options, $default );
        
        //Output
        echo '<textarea rows="'.$options['rows'].'" placeholder="'.$options['placeholder'].'" '.($required ? 'required="required"' : '').' name="'.$name.'">'.get_option($name).'</textarea>';
    }
    
    /**
     * URL field
     * @param string $name Field slug
     * @param bool $required Optional. Default false  
     * @param array $_options Field options:
     *  - placeholder text Input placeholder. Default null
     */
    protected static function urlField($name, $required = false, $_options)
    {
        $default = array(
            'placeholder' => ''
        );
        
        $options = wp_parse_args( $_options, $default );
        
        //Output
        echo '<input placeholder="'.$options['placeholder'].'" type="url" '.($required ? 'required="required"' : '').' name="'.$name.'" value="'.get_option($name).'" />';
    }
    
    /**
     * Media inserter field contains media id/ids
     * @param string $name Field slug
     * @param bool $required Optional. Default false  
     * @param array $_options Field options:
     *  - multiple bool User can insert multiple media. Default false
     *  - preview_spacer string String or html appended to preview inserted media. Default <br/>
     */
    protected static function mediaInsertField($name, $required = false, $_options = [])
    { 
        //Default options
        $default = array(
            'multiple' => false,
            'preview_spacer' => '<br/>'
        );
        
        $hidden_inputs = '';
        
        $options = wp_parse_args( $_options, $default );

        $value = get_option($name);

        //If isset value
        if($value){
            
            //If value is array (multiple elements)
            if(is_array($value))
            {
                
                //Foreach iteration count
                $i = 1; 
                
                //Count values
                $values_c = count($value);
                
                //Loop values as file id
                foreach($value as $fid)
                {                    
                    //Append filename to preview
                    $preview .= basename(get_attached_file($fid));
                    
                    //If preview element is not last prepend spacer
                    if($i < $values_c){
                        $preview .= $options['preview_spacer'];
                    }
                    
                    //Append hidden inputs
                    $hidden_inputs .= '<input type="hidden" name="'.$name.'[]" value=\''.$fid.'\'>';
                    
                    //Iterate loop counter
                    $i++;                
                }
                
            }
            
            //If value is not array (single element)
            else
            {
                //Set filename as preview
                $preview = basename(get_attached_file($value));
                
                //Append single input to hidden inputs
                $hidden_inputs .= '<input type="hidden" name="'.$name.'" value=\''.$value.'\'>';
            }
        }
        
        //Output
        echo '<div class="webz-insert-file-inputs-wrap" style="display: none">'.$hidden_inputs.'</div>'
                . '<input type="button" class="webz-insert-file" data-preview-spacer="'.$options['preview_spacer'].'" data-input-slug="'.$name.'" data-multiple="'.($options['multiple']===true ? 'yes' : 'no').'" value="Select"/>'
                . '<span class="webz-insert-file-preview-wrap">'.$preview.'</span>';
    }
    
    /**
     * Checkbox field
     * @param string $name Field slug
     * @param bool $required Optional. Default false  
     * @param array $_options Field options:
     *  - checked bool Input is default checked. Default false
     *  - value mixed Checkbox field value. Default empty
     */
    protected static function checkboxField($name, $required = false, $_options)
    {
        //Default options
        $default = array(
            'checked' => false,
            'value' => ''
        );
        
        $options = wp_parse_args( $_options, $default );
        
        $value = get_option($name);
        
        //If value exists set as default
        if($value){
            $options['value'] = $value;
        }

        /* Input is checked when:
         * - isset value and value is equal checkbox value
         * - value does not exist and checkbox checked option is true
         */        
        $checked = ( ($value && $value==$options['value']) || ($value===false && ($options['checked'] === true )) ) ? true : false;
        
        //Ouptut
        echo '<input type="checkbox" '.($required ? 'required="required"' : '').' name="'.$name.'" value="'.$options['value'].'" '.($checked ? ' checked="checked"' : '').'/>';
    }
    
    /**
     * Checkboxes list
     * @param string $name Field slug
     * @param bool $required Optional. Default false  
     * @param array $items Items in list, sigle item should be array value => Label. Default empty array
     * @param array $_options Field options:
     *  - spacer string String appended to single checkbox. Default <br/>
     *  - default array Default checked values. Default empty array
     */
    protected static function checkboxListField($name, $required = false, $items = array(), $_options = array())
    {
        //Default options
        $default = array(
            'spacer' => '<br/>',
            'default' => array()
        );
        
        //Set checkboxes container with data-required parameter (used in js) contains string yes/no
        echo '<div class="webz-checkbox-list-field" data-required="'.($required ? 'yes' : 'no').'">';
        
        $options = wp_parse_args( $_options, $default );
        
        $value = get_option($name);

        //If there's any items
        if(count($items) > 0) {
            
            //Loop items by value and name
            foreach($items as $item_value => $item_name)
            {
                /**
                 * Checkbox is checked when:
                 * - isset value and item_value exists in values array
                 * - there's no value and item_value exists in default selected options array
                 */
                $checked = ( $value && is_array($value) && in_array($item_value, $value) || (!$value && in_array($item_value, $options['default'])) ) ? true : false;
                
                //Add checkbox to output
                echo '<input type="checkbox" value="'.$item_value.'" name="'.$name.'[]" '.($checked ? ' checked="checked"' : '').'/>'.$item_name.'&nbsp;&nbsp'.$options['spacer'];
            }
        }
        
        //End of fields container and output
        echo '</div>';
    }
    
    /**
     * Checkboxes list
     * @param string $name Field slug
     * @param bool $required Optional. Default false  
     * @param array $items Items in list, sigle item should be array value => Label
     * @param array $_options Field options:
     *  - multiple bool User can select multiple items. Default false
     *  - default array/string Default checked value/values. Default empty
     *  - start_empty bool Prepend empty item to dropdown. Default true
     *  - empty_text string Text on empty item on dropdown begin. Default "Please select"
     */
    protected static function dropdownField($name, $required = false, $items = array(), $_options = array())
    {
        //Default options
        $default = array(
            'multiple' => false,
            'default' => '',
            'start_empty' => true,
            'empty_text' => 'Please select'
        );
        
        $options = wp_parse_args( $_options, $default );
        
        //Select tag start
        echo '<select '.($required ? 'required="required"' : '').' name="'.$name.'[]" '.($options['multiple'] ? 'multiple="multiple"' : '').'>';
        
        $value = get_option($name);

        //If can't select multiple items
        if(!$options['multiple']){            
            //If options start_empty parameter is true
            if($options['start_empty']){
                //Prepend empty option with empty_text option
                echo '<option value="">'.$options['empty_text'].'</option>';
            }            
        }
        
        //If there's any items
        if(count($items) > 0) {

            //if default options is not array anyway set default as array with only one element
            if(!is_array($options['default'])){
                $options['default'] = array($options['default']);
            }

            //Loop items by item_value and item_name
            foreach($items as $item_value => $item_name)
            {               
                /**
                 * Item is selected when:
                 * - isset value and item_value exists in values array
                 * - there's no value and item_value exists in default options array 
                 */
                $selected = ( $value && is_array($value) && in_array($item_value, $value) || (!$value && in_array($item_value, $options['default'])) ) ? true : false;
                
                //Append option to select
                echo '<option value="'.$item_value.'" name="'.$name.'[]" '.($selected ? ' selected="selected"' : '').'/>'.$item_name.'</option>';
            }
            
        }
        
        //End of select and output
        echo '</select>';
    }
    
    /**
     * Posts picker
     * @param string $name Field slug
     * @param post_type Post type slug. Dafautl post
     * @param bool $required Optional. Default false  
     * @param array $_options Field options:
     *  - display string How to display posts - select, checkbox_list etc. Default checkbox_list
     *  - posts_args array Post args, same as here - https://developer.wordpress.org/reference/classes/wp_term_query/__construct/
     *  ## Other parameters are the same as in checkbox_list, select ##
     */
    protected function postsPickerField($name, $posts_type = 'post', $required = false, $_options = array())
    {
        //Check if post type exists
        if(post_type_exists($posts_type))
        {
            //If posts_args in options doesn't exists - set it as empty array
            if(!isset($_options['posts_args'])){
                $_options['posts_args'] = array(); 
            }

            //If someone make mistake and use 'post_args' instead 'posts_args' 
            if(isset($_options['post_args'])){

                //Assign post_args to posts_args
                $_options['posts_args'] = $_options['post_args'];

                //Remove useless variable
                unset($_options['post_args']);
            }

            //Default posts args
            $default_posts_args = array(
                'posts_per_page'   => -1,
                'offset'           => 0,
                'category'         => '',
                'category_name'    => '',
                'orderby'          => 'date',
                'order'            => 'DESC',
                'include'          => '',
                'exclude'          => '',
                'meta_key'         => '',
                'meta_value'       => '',
                'post_type'        => $posts_type,
                'post_mime_type'   => '',
                'post_parent'      => '',
                'author'	   => '',
                'author_name'	   => '',
                'post_status'      => 'publish',
                'suppress_filters' => true
            );

            //Default options
            $default = array(
                'display' => 'checkbox_list',
                'posts_args' => wp_parse_args($_options['posts_args'], $default_posts_args)
            );

            $options = array_replace_recursive( $default, $_options );

            //WP Get posts by custom posts_args
            $posts = get_posts($options['posts_args']);

            $items = array();

            //Loop posts
            foreach($posts as $post){
                //Add post to dropdown
                $items[$post->ID] = $post->post_title;
            }

            switch($options['display']){
                case 'checkbox_list':
                    self::checkboxListField($name, $required, $items, $options);
                    break;
                case 'dropdown':
                    self::dropdownField($name, $required, $items, $options);
                    break;
                default:
                    self::checkboxListField($name, $required, $items, $options);
            }
        }
        //Display error if post type doesn't exists
        else{
            echo '<span style="color: red">Post type doesn\'t exists.</span>';
        }
    }
    
    /**
     * Post types picker
     * @param string $name Field slug
     * @param bool $required Optional. Default false  
     * @param array $_options Field options:
     *  - display string How to display post types - select, checkbox_list etc. Default checkbox_list
     *  - exclude array Excluded posts types slugs from list. Default 'attachment','revision','nav_menu_item','custom_css','customize_changeset','acf','mb-post-type','mb-taxonomy','vc4_templates','wpcf7_contact_form','vc_grid_item'
     *  ## Other parameters are the same as in checkbox_list, select ##
     */
    protected function postTypePickerField($name, $required = false, $_options = array())
    {
        //Default options
        $default = array(
            'display' => 'dropdown',
            'exclude' => array('attachment','revision','nav_menu_item','custom_css','customize_changeset','acf','mb-post-type','mb-taxonomy','vc4_templates','wpcf7_contact_form','vc_grid_item')
        );

        $options = array_replace_recursive( $default, $_options );

        //WP Get posts by custom posts_args
        $post_types =  get_post_types();

        $items = array();

        //Loop post types
        foreach($post_types as $post_type => $post_type_name){
            //If post type is not excluded - add to dropdown
            if(!in_array($post_type, $options['exclude'])){
                $items[$post_type] = ucfirst($post_type_name);
            }            
        }

        //Select display method
        switch($options['display']){
            case 'dropdown':
                self::dropdownField($name, $required, $items, $options);
                break;
            case 'checkbox_list':
                self::checkboxListField($name, $required, $items, $options);
                break;
            default:
                self::dropdownField($name, $required, $items, $options);
        }
    }
    
    /**
     * Taxonomy items picker
     * @param string $name Field slug
     * @param bool $required Optional. Default false  
     * @param array $_options Field options:
     *  - display string How to display post types - select, checkbox_list etc. Default checkbox_list
     *  - depth_spacer strin Word or character displayed before item in list multiplied by depth value. Default ' - '
     *  ## Other parameters are the same as in checkbox_list, select ##
     */
    protected function taxonomyItemsPickerField($name, $taxonomy, $required = false, $_options = array())
    {
        //Default options
        $default = array(
            'display' => 'dropdown',
            'depth_spacer' => ' - '
        );

        $options = array_replace( $default, $_options );

        //WP Get posts by custom posts_args
        $taxonomy_items = webz_generate_taxonomies_array($taxonomy);

        $items = array();

        //Loop taxonomy items
        foreach($taxonomy_items as $item_id => $item){
            
            //Depth spacer displayed before item on list
            $depth_spacer = '';
            
            //If depth is greater than 0
            if($item->depth > 0){
                //Add option depth_spacer * item_dempt to depth_spacer variable
                for($i=0;$i<$item->depth;$i++){
                    $depth_spacer .= $options['depth_spacer'];
                }                
            }
            
            $items[$item_id] = $depth_spacer.$item -> name;           
        }

        //Select display method
        switch($options['display']){
            case 'dropdown':
                self::dropdownField($name, $required, $items, $options);
                break;
            case 'checkbox_list':
                self::checkboxListField($name, $required, $items, $options);
                break;
            default:
                self::dropdownField($name, $required, $items, $options);
        }
    }

}