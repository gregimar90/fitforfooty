(function($){
    
    if(webz_gallery_sliders.length > 0){
        $.each(webz_gallery_sliders, function(i, slider){
            $('#'+slider.id).owlCarousel({
                loop: slider.loop,
                margin:10,
                nav: slider.nav,
                autoplay: slider.autoplay,
                autoplayTimeout: slider.autoplayTimeout,
                autoplayHoverPause: slider.autoplayHoverPause,
                mouseDrag: true,
                touchDrag: false,
                responsive:{
                    0: {
                        items: slider.items_per_page_xsmall,
                        mouseDrag: false,
                        touchDrag: true
                    },
                    576:{
                        items: slider.items_per_page_xsmall,
                        mouseDrag: false,
                        touchDrag: true
                    },
                    768:{
                        items: slider.items_per_page_small,
                        mouseDrag: false,
                        touchDrag: true
                    },
                    922:{
                        items: slider.items_per_page_medium
                    },
                    1200:{
                        items: slider.items_per_page_large
                    }
                }
            });
        });
    }    
    
})(jQuery);