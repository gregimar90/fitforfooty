<?php
$term = get_queried_object();

$webz_news_category_id = $term -> term_id;

if( isset( $_GET['ajax'] ) ) :
    echo do_shortcode( '[webz_news default_category_id="' . $webz_news_category_id . '"]' );    
else:

    get_header();
    
    $page = get_post( NEWS_PAGE_ID );
    $page_css = get_field( '_wpb_shortcodes_custom_css', $page -> ID );

    if( $page_css ) {
        add_action( 'wp_footer', function() use ( $page_css ) {
            echo '<style>' . $page_css . '</style>';
        } );
    }

    echo apply_filters( 'the_content', $page -> post_content );  

    get_footer();

endif;